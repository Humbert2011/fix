<?php
namespace App\Model;

use App\Lib\Response,
    APP\Lib\Cifrado,
    App\Lib\Push;

class CotizacionModel
{
    private $db;
    private $table = 'cotizacion';
    private $tableSec = 'detallecotizacion';
    private $tableTer = 'persona';
    private $tableCua = 'solicitud';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    { 
        $data = $this->db->from($this->table)
                         ->orderBy('Id DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
    public function listarCotizaciones($u)
    {
      $data = $this->db->from($this->table)
                         ->where('IdSolicitudServicio',$u)
                         ->orderBy('Id DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
    public function obtener($id)
    {

        $buscar =  $this->db->from($this->table,$id)
                            ->fetch();//para un solo dato o linea

        if ($buscar != false) {            
            $detalle = $this->db->from($this->table)                                
                                ->leftJoin('detallecotizacion ON detallecotizacion.IdCotizacion = cotizacion.id')
                                ->select('detallecotizacion.Id, detallecotizacion.Descripcion,detallecotizacion.Monto, detallecotizacion.FechaCreacion as FechaDetalle')                                
                                ->where('cotizacion.Id',$id)
                                ->fetchAll();//para un solo dato o linea

            $output = array();
            for ($i=0; $i < count($detalle) ; $i++) { 
              $output[] = array('Id' => $detalle[$i]->Id,
                                'Descripcion' => $detalle[$i]->Descripcion,
                                'Monto' => $detalle[$i]->Monto,
                                'FechaDetalle' => $detalle[$i]->FechaDetalle);
            }

            $this->response->result = ['cotizacion'=>$buscar,
                                       'detalle'=>$output
                                      ];
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='La cotizacion no existe';
            return $this->response->SetResponse(false);
         }
    }

    public function registrar($data)
    {

        $insertarCotizacion = $this->db->insertInto($this->table, $data)
                 ->execute();
                 
        $buscar = $this->db->from($this->table)
                           ->leftJoin('solicitud ON solicitud.Id = cotizacion.IdSolicitudServicio')
                           ->leftJoin('persona ON persona.Id = solicitud.IdCliente')
                           ->leftJoin('token ON token.IdUsuario = persona.Id')
                           ->where('cotizacion.Id',$insertarCotizacion)
                           ->select('token.Token')
                           ->fetch()
                           ->Token;
                           
                           $actualizarSolisitud = $this->db->update($this->tableCua,array('IdAtendidaPor'=> $data['IdCreadaPor'],'StatusSolicitud'=> '1'), $data['IdSolicitudServicio'])#actualizar estadoa a 1
                                  ->execute();

                           $data = array("id"=>$insertarCotizacion);

                           $Push = Push::FMC('Solicitud Cotizada','La solicitud fue cotizada', $buscar,$data);#envio de notificacion

               $this->response->result=$insertarCotizacion;
        return $this->response->SetResponse(true);

    }


    //
    public function rechazar($data,$id)
    {
        
        $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {

            $actualizar= $this->db->update($this->table,array('Aprobada'=> '0' ), $id)
                                  ->execute();
            #regresa el vaor del estado a 0
            $new_data = array('IdCotizacion' => $id);
            $data = array_merge($data, $new_data);
            $insertarDetalleCotizacion = $this->db->insertInto($this->tableSec, $data)
                                                  ->execute();
              #push al admin                      
            $buscarAdmin = $this->db
                         ->from($this->tableTer)
                         ->leftJoin('token ON persona.Id = token.IdUsuario')
                         ->select('Token')
                         ->where('Activo = 1 AND TipoPersona = "Admin"') 
                         ->limit(1)
                         ->fetch('Token');
                         $data = array('id' => $insertarSolicitud ); 
            $Push = Push::FMC('Solicitud',$mensaje,$buscarAdmin,$data);
            #fin de push
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);

          }else{
            $this->response->errors[]='La cotizacion no existe';
          return $this->response->SetResponse(false);
          }
    }
    //
    public function aceptar($id)
    {
        
        $buscar = $this->db->from($this->table,$id)
                      ->select('IdSolicitudServicio')
                      ->fetch()
                      ->IdSolicitudServicio;
          if ($buscar > 0) {
              $actualizar= $this->db->update($this->table,array('Aprobada'=> '1' ), $id)
                                  ->execute();
            #actualizar a 2 estado del servicio
            $actualizarSolisitud = $this->db->update($this->tableCua,array('StatusSolicitud'=> '2'), $buscar)#actualizar estadoa a 1
                                  ->execute();
            #push al admin                      
            $buscarAdmin = $this->db
                         ->from($this->tableTer)
                         ->leftJoin('token ON persona.Id = token.IdUsuario')
                         ->select('Token')
                         ->where('Activo = 1 AND TipoPersona = "Admin"') 
                         ->limit(1)
                         ->fetch('Token');
                         $data = array('id' => $id );
                         $mensaje = 'Cotizacion';
            $Push = Push::FMC('Solicitud',$mensaje,$buscarAdmin,$data);
            #fin de push
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);

          }else{
            $this->response->errors[]='La cotizacion no existe';
          return $this->response->SetResponse(false);
          }
    }

    public function actualizar($data,$id)
    {
       $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {
            $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
          }else{
            $this->response->errors[]='La cotizacion no existe';
          return $this->response->SetResponse(false);
          }
    }

    public function eliminar($id)
    {
         $eliminar = $this->db->deleteFrom($this->table,$id)
                 ->execute();
                 $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>