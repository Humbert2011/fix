<?php
namespace App\Model;

use App\Lib\Response,
    APP\Lib\Cifrado;

class DetalleservicioModel
{
    private $db;
    private $table = 'detalleservicio';
    private $table2 = 'incidenciaservicio';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    //no cambios
    public function listar($u)
    { 
        $data = $this->db->from($this->table)
                         ->where('IdServicio',$u)
                         //->orderBy('Id DESC')
                         ->fetchAll();//para mas de un registro        
        $this->response->result = [
            'data'  => $data
        ];
        return $this->response->SetResponse(true);
    }

    public function obtener($id)
    {

        $buscar =  $this->db->from($this->table,$id)
                    ->fetch();//para un solo dato o linea

        if ($buscar != false) {
            $this->response->result = $buscar;
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='el detalle de servicio no existe';
            return $this->response->SetResponse(false);
         }

    }

    public function registrar($data)
    {
        $insertarDireccion = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result=$insertarDireccion;
        return $this->response->SetResponse(true);

    }

    public function actualizar($data,$id)
    {
       $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {
            $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
          }else{
            $this->response->errors[]='el detalle de servicio no existe';
          return $this->response->SetResponse(false);
          }
    }

    public function eliminar($id)
    {
         $eliminar = $this->db->deleteFrom($this->table,$id)
                 ->execute();
                 $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>