<?php
namespace App\Model;

use App\Lib\Response;

class DireccionModel
{
    private $db;
    private $table = 'direccion';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    #lsitar direcciones
    public function listar($user )
    {
        $data = $this->db->from($this->table)
                         ->where('IdPropietario',$user)
                         ->where('Guardada',true)
                         ->orderBy('Id DESC')
                         ->fetchAll();

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->where('IdPropietario',$user)
                          ->where('Guardada',true)
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
    #obtener direccion
    public function obtener($id)
    {

        $buscar =  $this->db->from($this->table,$id)
                    ->fetch();

        if ($buscar != false) {
            $this->response->result = $buscar;
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='La direccion no se encuentra';
            return $this->response->SetResponse(false);
         }

    }
    #alta de direccion
    public function registrar($data)
    {
        $insertarDireccion = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result=$insertarDireccion;
        return $this->response->SetResponse(true);
    }
    #actualizar direccion
    public function actualizar($data,$id)
    {
        $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {
            $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
          }else{
            $this->response->errors[]='La direccion no existe';
          return $this->response->SetResponse(false);
          }
    }
    #actualizar direcion defecto
    public function defecto($idDireccion,$idUser)
    {
        $limpiar = $this->db->update($this->table)
                               ->set('Defecto',0)
                               ->where("IdPropietario",$idUser)
                               ->execute();

        $actualizar = $this->db->update($this->table)
                               ->set('Defecto',1)
                               ->where("Id",$idDireccion)
                               ->execute();

       $this->response->result = $actualizar;
              return $this->response->SetResponse(true);                        
    }
    #eliminar
    public function eliminar($id)
    {
        $eliminar = $this->db->deleteFrom($this->table,$id)
                 ->execute();
                 $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>