<?php 
namespace App\model;

use App\Lib\Response,
	App\Lib\Cifrado;

	class EspecialistaModel
	{
		private $db;
		private $table = 'persona';
		private $response;
		private $esp = 'Especialista';

		public function __CONSTRUCT($db)
		{
			$this->db = $db;
			$this->response = new Response();
		}

		public function obtener($id){
			$buscar = $this->db->from($this->table,$id)
							   ->where('persona.TipoPersona',$this->esp)
							   ->fetch();
			if ($buscar != false) {
				$this->response->result = $buscar;
	            return $this->response->SetResponse(true);
	         }else{
	            $this->response->errors = 'El Id no es de un especialista';
	            return $this->response->SetResponse(false);
	         }
		}

		public function listar($l,$p){
			$data = $this->db->from($this->table)
							 ->where('persona.TipoPersona',$this->esp)
							 ->limit($l)
							 ->offset($p)
							 ->fetchAll();

			$total = $this->db->from($this->table)
                          	 ->select('COUNT(*) Total')
                          	 ->where('persona.TipoPersona',$this->esp)
                          	 ->fetch()
                          	 ->Total;

			$this->response->result = [
				'data' => $data,
				'especialistas' => $total
			];
			return $this->response->SetResponse(true);
		}

		public function listars(){
			$data = $this->db->from($this->table)
			                 ->where('persona.TipoPersona',$this->esp)
			                 ->fetchall();
			$total = $this->db->from($this->table)
                          	 ->select('COUNT(*) Total')
                          	 ->where('persona.TipoPersona',$this->esp)
                          	 ->fetch()
                          	 ->Total;

			$this->response->result = [
				'data' => $data,
				'especialistas' => $total
			];
			return $this->response->SetResponse(true);
		}

		public function registrar($data,$TipoPersona){
			$email = $data['email'];
			$telefonoCelular = $data['TelefonoCelular'];
			$buscar = $this->db->from($this->table)
							   ->select(array('email','TelefonoCelular'))
							   ->where('(TelefonoCelular = :telefono OR email = :email) AND Activo = 1',
										array(':telefono' => $telefonoCelular, ':email' => $email))
							   ->execute()
                          	   ->fetchAll();
			if (count($buscar)>0) {
				$this->response->errors[]='El correo o telefono ya está ligado a una cuenta';
				return $this->response->SetResponse(false);
			} else {
				$new_data = array('TipoPersona' => $TipoPersona);
                $data = array_merge($data, $new_data);
                $data['Password'] = Cifrado::BLOWFISH($data['Password']);
                $insertEspecialista = $this->db->insertInto($this->table, $data)
                                          ->execute();
                       $this->response->result=$insertEspecialista;
                return $this->response->SetResponse(true);				
			}			
		}

		public function actualizar($data,$id){
            $email = $data['email'];
	        $telefonoCelular = $data['TelefonoCelular'];
			$buscar2 = $this->db->from($this->table)			
			 	   		            //->select(array('email','TelefonoCelular'))
							        ->where('(TelefonoCelular = :telefono OR email = :email) AND Activo = 1 AND Id != :idE',
									       array(':telefono' => $telefonoCelular, ':email' => $email, 'idE' => $id))
							        ->execute()
							        ->fetchAll();
			    if(count($buscar2)>0){
			    	//Aquí encontró un registro en un id que no es el que se está mandando
			    	$this->response->errors[]='El correo o telefono ya está ligado a una cuenta';
			        return $this->response->SetResponse(false);
			    }else{
			    	$actualizar = $this->db->update($this->table, $data, $id)
	    	                               ->execute();
	                       $this->response->result = $actualizar;
		            return $this->response->SetResponse(true);
			    }
		}

		public function eliminar($id)
	    {
	         $eliminar = $this->db->deleteFrom($this->table,$id)
	                 ->execute();
	                 $this->response->result = $eliminar;
	         return $this->response->SetResponse(true);
	    }
	}

 ?>