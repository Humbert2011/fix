<?php
namespace App\Model;

use App\Lib\Response;

class ImgModel
{
	private $db;
   #	private $table;
    private $response;
    private $tbPersona = 'persona';
    private $dir_server = 'http://localhost/fix/img/';#local
    private $dir_subida = '/var/www/html/fix/img';#local

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    #lsitar imagenes
    public function listar($l, $p , $user )
    {
        $data = $this->db->from($this->table)
                         ->where('IdSolicitud',$user)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('Id DESC')
                         ->fetchAll();

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
     #obtener imagenes
    public function obtener($id)
    {

        $buscar =  $this->db->from($this->table,$id)
                    ->fetch();

        if ($buscar != false) {
            $this->response->result = $buscar;
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='La direccion no se encuentra';
            return $this->response->SetResponse(false);
         }

    }
    #cargar
    public function cargar($file)
    {
		$fecha = date('Y-m-d H:i:s');//fecha de subida
		$t = explode("_",$file['img']['name']);
		$table = $t[0];
		$id =explode('.',$t[1]);
		$id =$id[0];
		#variables de subida ruta general
    	
    $dir_subida = $this->dir_subida;#ruta de subida local
    #$dir_server = 'http://fikz.stardust.com.mx/img/';
    $dir_server = $this->dir_server;
    #$dir_subida ='/home/stardust007/public_html/fikz/img/';#ruta de subida en linea
    #agregar linea de ubicacion en el servidor 
    $dir_subida .= $table.'/';
    $dir_server .= $table.'/';
    #ruta del archivo		
		$fichero_subido = $dir_subida . basename($file['img']['name']);
        $NombreArchivo = $dir_server . basename($file['img']['name']);
    	#asignacion de tabla
		switch ($table) {
		    case "cotizacion":
		        $table = 'fotodetallecotizacion';
		        $data = array('IdDetalleCotizacion' => $id,'FechaDeCarga' => $fecha,'NombreArchivo' => $NombreArchivo);
		        break;
		    case "servicio":
		        $table = 'fotodetalleservicio';
		        $data = array('IdDetalleServicio' => $id,'FechaDeCarga' => $fecha,'NombreArchivo' => $NombreArchivo);
		        break;
		    case "incidencia":
		        $table = 'fotoincidencia';
		        $data = array('FotoDetalleServicio_Id' => $id,'FechaDeCarga' => $fecha,'NombreArchivo' => $NombreArchivo);
		        break;
		    case "solicitud":
		        $table = 'fotosolicitud';
		        $data = array('IdSolicitud' => $id,'FechaDeCarga' => $fecha, 'NombreArchivo' => $NombreArchivo);
		        break;
		    case "tiposervicio":
                $table = 'fototiposervicio';
                $data = array('IdTipoServicio' => $id,'FechaDeCarga' => $fecha, 'NombreArchivo' => $NombreArchivo);
                break;    
		}#tipoServicio
		try{
		    move_uploaded_file($file['img']['tmp_name'], $fichero_subido);
			if($table === 'persona'){
				  $actualizar= $this->db->update($table, $data, $id)
                         	 	->execute();
	              $this->response->result = $actualizar;
	              return $this->response->SetResponse(true,'Se subio la foto de perfil del usuario');
			}else{
				$insertarImg = $this->db->insertInto($table, $data)
	                 			->execute();
			    $this->response->result = $insertarImg;
            	return $this->response->SetResponse(true,'El fichero es válido y se subió con éxito');
            }
         } catch (Exception $e) {
            #echo ;
            $this->response->errors[]='Error encontrado: '.  $e->getMessage(). "\n";
            return $this->response->SetResponse(false);
         }	
		// $this->response->result = $data;
         // return $this->response->SetResponse(true,'El fichero es válido y se subió con éxito.\n');
    }

  public function cargarfotoperfil($file,$id){    
    $max_upload = false;
    if ($file['img']['error'] > 0 or $file['img']['tmp_name']=="") {
             $this->response->errors = 'Tenemos problemas al cargar el archivo';
      return $this->response->SetResponse(false);
      
    }else{

      $dir_subidap = $this->dir_subida;#local
      $dir_serverp = $this->dir_server;#local
      #variables de subida ruta general
      // buscar $id para saber si es update o insert
      $fichero_subidap = $dir_subidap.'perfilusuario/'.$id.'-'.basename($file['img']['name']);
      $nombre_archivop = $dir_serverp.'perfilusuario/'.$id.'-'.basename($file['img']['name']);
    
      $metodo = $this->db->from($this->tbPersona)
                              ->select('COUNT(*) as num')
                              ->where('Id',$id)
                              ->fetch('num');
      $data = array('FotoPerfil' => $nombre_archivop);
       
      $ex = true;
      switch ($metodo) {
        case 0:
          $updateUrl = $this->db->insertInto($this->tbPersona,$data)
                                ->where('Id',$id)
                                ->execute();       
          break;
        case 1:
          $updateUrl = $this->db->update($this->tbPersona,$data)
                                ->where('Id',$id)
                                ->execute();
          break;
        default:
          $updateUrl = $this->db->update($this->tbPersona,$data)
                                ->where('Id',$id)
                                ->execute();

      }

      #alta de archivos 
      try{
        move_uploaded_file($file['img']['tmp_name'], $fichero_subidap);
        if ($ex) {
                 $this->response->result = $updateUrl;
                 $this->response->errors='';
          return $this->response->SetResponse(true);
        } else {
                 $this->response->errors = $error;
          return $this->response->SetResponse(false);
        }
      }catch(Exception $e){
               $this->response->errors='Error encontrado: '.  $e->getMessage(). "\n";
        return $this->response->SetResponse(false);
      }
    }
  }

}	