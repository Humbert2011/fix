<?php
namespace App\Model;

use App\Lib\Response,
    APP\Lib\Cifrado;

class IncidenciaservicioModel
{
    private $db;
    private $table = 'incidenciaservicio';
    private $table2 = 'detalleservicio';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    //no cambios
    public function listar($u)
    { 
        $data = $this->db->from($this->table2)
                         ->where('IdServicio',$u)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('Id DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }

    public function obtener($id)
    {

        $buscar =  $this->db->from($this->table,$id)
                    ->fetch();//para un solo dato o linea

        if ($buscar != false) {
            $this->response->result = $buscar;
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='el detalle de servicio no existe';
            return $this->response->SetResponse(false);
         }

    }

    public function registrar($data)
    {
        $insertarDireccion = $this->db->insertInto($this->table2, $data)
                 ->execute();
               $this->response->result=$insertarDireccion;
        return $this->response->SetResponse(true);

    }

    public function actualizar($data,$id)
    {
       $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {
            $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
          }else{
            $this->response->errors[]='el detalle de servicio no existe';
          return $this->response->SetResponse(false);
          }
    }

    public function eliminar($id)
    {
         $eliminar = $this->db->deleteFrom($this->table,$id)
                 ->execute();
                 $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>