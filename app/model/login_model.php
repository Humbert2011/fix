<?php
namespace App\Model;
use App\Lib\Response,
    App\Lib\Login,
    App\Lib\Cifrado,
    App\Lib\Codigos,
    App\Lib\Email,
    App\Lib\SMS;

class LoginModel
{
    private $db;
    private $table = 'persona';
    private $tableSecundaria = 'mensajes';
    private $tableTer = 'codigoEspecialista';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    #autentificacion del usuario
    public function autenticar($telefonoCelular, $password,$TipoPersona) {
        $password = Cifrado::BLOWFISH($password);
        $persona = $this->db->from($this->table)
                             ->where('TelefonoCelular', $telefonoCelular)
                             ->where('Password', $password)
                             ->where('TipoPersona',$TipoPersona)
                             ->fetch();

        if(is_object($persona)){
            //$nombre = explode(' ', $persona->Nombre)[0];

            $token = Login::SignIn([
                'id' => $persona->Id,
                'Nombre' => $persona->Nombre,
                'ApellidoPaterno' =>$persona->ApellidoPaterno,
                'ApellidoMaterno' =>$persona->ApellidoMaterno,
                'TelefonoCelular' =>$persona->TelefonoCelular, 
                'email' =>$persona->email,
                'FotoPerfil' => $persona->FotoPerfil,
                'TipoPersona' => $persona->TipoPersona
                //'NombreCompleto' => $persona->Nombre,
                //'Imagen' => $persona->Imagen, LO DEJAMOS COMENTADO PORQUE HACE GENERAR UN TOKEN DEMASIADO GRANDE
                //'EsAdmin' => (bool)$persona->EsAdmin
            ]);
            #codigo para validacion|
            if ($persona->TipoPersona === 'Especialista') {
                $codigo  = Codigos::Codigo(20);
                #alta en base de datos
                #buscar id en tabla
                $buscar = $this->db->from($this->tableTer)
                      ->select('COUNT(*) Num')
                      ->where('IdPersona',$persona->Id)
                      ->fetch()
                      ->Num;
                if ($buscar > 0) {
                    $actulizar = $this->db->update($this->tableTer,array('Codigo'=>$codigo))
                                          ->where('IdPersona',$persona->Id)
                                          ->execute();
                }
                else{
                    $insert = $this->db->insertInto($this->tableTer,array('IdPersona' =>$persona->Id,'Codigo'=>$codigo))
                                       ->execute();
                }
                #no insert
                $this->response->result = ['token' =>  $token,
                                       'id'    =>  $persona->Id,
                                       'Codigo'=>  $codigo 
                                      ];
            }else{
            $this->response->result = ['token' =>  $token,
                                       'id'    =>  $persona->Id
                                      ];
            }
            return $this->response->SetResponse(true);
        }else{
            return $this->response->SetResponse(false, "Credenciales no válidas");
        }
    }
    #validacion de numero
    public function validarNumero($codigoPais,$telefonoCelular)
    {
        $buscar =  $this->db->from($this->table)
                    ->select('COUNT(*) as Num')
                    ->where('TelefonoCelular',$telefonoCelular)
                    ->fetch()//para un solo dato o linea
                    ->Num;
        if ($buscar === "0") {
            // generar codigo SMS
            $codigo = Codigos::Codigo(4);
            #enviar codigo SMS
            $send = SMS::SendMensaje($codigoPais,$telefonoCelular,$codigo);
            $send = '1';
            if ($send == '1') {
                $fecha = date('Y-m-d H:i:s');
                $fechaEx = date('Y-m-d H:i:s', (strtotime ("+1 Hours")));
                $data = [
                    "IdTipo"=> 1,
                    "FechaEnvio" => $fecha,
                    "Mensaje" => "Su codigo de verificacion es: ",
                    "FechaExpiracion" => $fechaEx,
                    "Codigo"=>$codigo,
                    "TempTelefono"=>$telefonoCelular

                ];
                //alata de codigo en base de datos
                $altaCodigo = $this->db->insertInto($this->tableSecundaria, $data)
                     ->execute();
                $this->response->result=$altaCodigo;
                return $this->response->SetResponse(true,"Codigo enviado al telefono: ".$telefonoCelular);
            }else{
                $this->response->errors[]='Error al enviar mensaje SMS al numero'.$telefonoCelular.' indetentelo de nuevo.';
                return $this->response->SetResponse(false);
            }
            
         }else{
            $this->response->errors[]='El usuario ya existe';
            return $this->response->SetResponse(false);
         }
    }
    #validacion del codigo de registro
    public function validarCodigo($telefonoCelular,$codigo){
        $codigoDB = $this->db->from($this->tableSecundaria)
                                ->select('Codigo')
                                ->where('Id = (SELECT MAX(Id) FROM mensajes WHERE IdTipo = 1 AND TempTelefono = :telefono )',array(':telefono' => $telefonoCelular))
                                ->execute()
                                ->fetch()
                                ->Codigo;
        if ($codigoDB > 0 ) {
            if ($codigoDB === $codigo ) {
            $this->response->result=$codigo;
                    return $this->response->SetResponse(true,"Codigo de verificacion correcto");
            }else{
                $this->response->errors[]='El codigo que ingreso es incorrecto';
                return $this->response->SetResponse(false); 
            }
        }else{
            $this->response->errors[]='No hay codigo para este telefono: '.$telefonoCelular;
                return $this->response->SetResponse(false);
        }        
    }
    
    //Valida que el usuario esté registrado y esté entre el rango de la Fecha de Expiracion
    public function validaUser($id){
        $buscaU = $this->db->from($this->table, $id)
                           ->fetch();
        if($buscaU != false){
            $actual =  date('Y-m-d H:i:s');
            $busca = $this->db->from($this->tableSecundaria)
                              ->where('Id = (SELECT MAX(Id) FROM mensajes WHERE IdTipo = 2)')
                              ->where('IdDestinatario',$id)
                              ->where('Status = 1')
                              ->where('? <= FechaExpiracion',$actual)
                              ->execute()
                              ->fetch();
            if($busca != false){
                    $this->response->result=$busca;
                return $this->response->SetResponse(true, 1);
            }else{
                    $this->response->errors='No esta asignado';
                return $this->response->SetResponse(false, 0);
            }            
        }else{
            $this->response->errors='El id no existe';
            return $this->response->SetResponse(false, 0);
        }
    }

    //Valida el codigo que se ha enviado desde la web
    public function validaC($id,$codigo){
        $actual =  date('Y-m-d H:i:s');
        $buscaC = $this->db->from($this->tableSecundaria)
                           ->where('Id = (SELECT MAX(Id) FROM mensajes WHERE IdTipo = 2)')
                           ->where('IdDestinatario',$id)
                           ->where('Status = 1')
                           ->where('Codigo',$codigo)
                           ->where('? <= FechaExpiracion',$actual)
                           ->execute()
                           ->fetch();
        if($buscaC != false){
                   $this->response->result=$busca;
            return $this->response->SetResponse(true, 1);
        }else{
                   $this->response->errors='El codigo no es correcto';
            return $this->response->SetResponse(false, 0);
        }
    }
    
    //Actualizar el password
    public function ActualizaPass($id,$data){
        $buscaU = $this->db->from($this->table,$id)
                           ->fetch();
        if ($buscaU != false) {
            if(isset($data['Password'])){
                $data['Password'] = Cifrado::BLOWFISH($data['Password']);
            }
            $actualiza = $this->db->update($this->table,$data,$id)
                                  ->execute();

            $this->response->result=$buscaU;
            return $this->response->SetResponse(true,1);
        } else {
            $this->response->errors='El id no existe';
            return $this->response->SetResponse(false,0);
        }
        
    }

    #recuperacion de password
    public function recuperarPassword($email){
        $buscar =  $this->db->from($this->table)
                    ->select('Id')
                    ->where('email',$email)
                    ->fetch()//para un solo dato o linea
                    ->Id;
        if ($buscar > 0) {
            //codigo de recuperacion de password
            $codigo = Codigos::Codigo(6);
            #enviar codigo por Email
            $send = Email::Send($email,$codigo,$buscar);
            if ($send == '1') {
                $fecha = date('Y-m-d H:i:s');
                $fechaEx = date('Y-m-d H:i:s', (strtotime ("+24 Hours")));
                $data = [
                    "IdDestinatario"=>$buscar,
                    "IdTipo"=> 2,
                    "FechaEnvio" => $fecha,
                    "Mensaje" => "Su codigo de verificacion es: ",
                    "FechaExpiracion" => $fechaEx,
                    "Codigo"=>$codigo
                ];
                //alata de codigo en base de datos
                $altaCodigo = $this->db->insertInto($this->tableSecundaria, $data)
                     ->execute();
                $this->response->result=$altaCodigo;
                return $this->response->SetResponse(true,"Codigo enviado al emali: ".$email);

            }else{
                $this->response->errors[]='Error al enviar email porfavor intentelo de nuevo.';
                return $this->response->SetResponse(false);
            }
         }else{
            $this->response->errors[]='El email no esta asociado a ninguna cuenta';
            return $this->response->SetResponse(false);
         }
    }

    public function getData($token)
    {       
            $data = Login::GetData($token);
            if ($data === "Signature verification failed") {
               $this->response->errors[]='token incorrecto';
                return $this->response->SetResponse(false);
            }else{
                $this->response->result=$data;
                return $this->response->SetResponse(true);
            }
           
        #return $data;
    }

}
