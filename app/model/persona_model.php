<?php
namespace App\Model;

use App\Lib\Response,
    APP\Lib\Cifrado;

class PersonaModel
{
    private $db;
    private $table = 'persona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($l, $p)
    { 
        $data = $this->db->from($this->table)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('Id DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }

    public function obtener($id)
    {
        $buscar =  $this->db->from($this->table,$id)
                    ->fetch();//para un solo dato o linea

        if ($buscar != false) {
            $this->response->result = $buscar;
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='El usuario no existe';
            return $this->response->SetResponse(false);
         }
    }

    public function registrar($data,$TipoPersona)
    {
        $email = $data['email'];
        $telefonoCelular = $data['TelefonoCelular'];
        $buscar = $this->db->from($this->table)
                          ->select(array('email', 'TelefonoCelular'))
                          ->where('(TelefonoCelular = :telefono OR email = :email) AND Activo = 1',
                            array(':telefono' => $telefonoCelular,':email'=> $email))
                          ->execute()
                          ->fetchAll();
      if(count($buscar)>0){
        $this->response->errors[]='El Correo o el Telefono ya esta ligado a una cuenta';
        return $this->response->SetResponse(false);
      }else{
        $new_data = array('TipoPersona' => $TipoPersona);
        $data = array_merge($data, $new_data); 
        $data['Password'] = Cifrado::BLOWFISH($data['Password']);
        $insertPersona = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result=$insertPersona;
        return $this->response->SetResponse(true);
      }
    }

    public function regFacebook($data,$TipoPersona)
    {
        $buscar = $this->db->from($this->table)
                           ->where('IdFacebook',$data['IdFacebook'])
                           ->where('TipoPersona',$TipoPersona)
                           ->fetch();
        if ($buscar!=false) {
                   $this->response->errors[]='El id de Facebook ya esta ligado a una cuenta';
            return $this->response->SetResponse(false);
        }else{
            $new_data = array('TipoPersona' => $TipoPersona);
            $data = array_merge($data, $new_data); 
            $altaFacebook = $this->db->insertInto($this->table,$data)
                                     ->execute();
                   $this->response->result=$altaFacebook;
            return $this->response->SetResponse(true); 
        }
    }

    public function actualizar($data,$id)
    {
      $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
      if ($buscar > 0) {
          if (isset($data['Password'])) {
          $data['Password'] = Cifrado::BLOWFISH($data['Password']);
        }

          $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
      }else{
          $this->response->errors[]='El Usuario no existe';
          return $this->response->SetResponse(false);
      }
    }

    public function eliminar($id)
    {
         $eliminar = $this->db->deleteFrom($this->table,$id)
                 ->execute();
                 $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
