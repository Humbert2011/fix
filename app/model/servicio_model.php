<?php
namespace App\Model;

use App\Lib\Response,
    APP\Lib\Cifrado,
    App\Lib\Push,
    App\Lib\Login;

class ServicioModel
{
    private $db;
    private $table = 'servicio';
    private $tableSec = 'solicitud';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    public function listaEspecialista($id)
    {      
          $data = $this->db  ->from($this->table)
                             ->select('servicio.Id, solicitud.Descripcion,fotosolicitud.NombreArchivo as Foto, solicitud.RequiereVisita, solicitud.Sos, solicitud.IdTipoServicio,tiposervicio.Descripcion AS TipoServicio,subtiposervicio.Descripcion AS SubtipoServicio,CONCAT(direccion.Calle," ",direccion.NoExt," ",direccion.NoInt," ",direccion.CP," ",direccion.Colonia," ",IFNULL(direccion.MunicipioDelegacion,"")," ",direccion.Ciudad," ",direccion.Estado) AS DireccionServicio,solicitud.IdCliente,CONCAT(persona.Nombre," ",persona.ApellidoPaterno) AS NombreCliente,solicitud.FechaDeseadaServicio AS FechaDeseada, solicitud.IdEspecialistaAsignado AS IdEspecialista,CONCAT(personaE.Nombre, " ",personaE.ApellidoPaterno) AS NombreEspecialista')
                             ->leftJoin('solicitud ON solicitud.id = servicio.IdSolicitud')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->leftJoin('tiposervicio ON solicitud.IdTipoServicio = tiposervicio.Id')
                             ->leftJoin('subtiposervicio ON solicitud.IdSubTipoServicio = subtiposervicio.Id')
                             ->leftJoin('direccion ON solicitud.IdDireccionServicio = direccion.Id')
                             ->leftJoin('persona ON solicitud.IdCliente = persona.Id')
                             ->leftJoin('persona AS personaE ON solicitud.IdEspecialistaAsignado = personaE.Id')
                             ->where('servicio.IdStatusServicio < 6')#cambio de numero de estatus
                             ->where('solicitud.IdEspecialistaAsignado',$id)
                             //->orderBy('Id DESC')
                             ->fetchAll();//para mas de un registro


                 $this->response->result =[ 
                      "data" => $data
                 ];            
          return $this->response->SetResponse(true);
    }

    public function listar($l,$p,$st)#cambio completo
    { 
        if ($st > 2) {
          $data = $this->db  ->from($this->table)
                             ->select('servicio.Id, solicitud.Descripcion,fotosolicitud.NombreArchivo as Foto')
                             ->leftJoin('solicitud ON solicitud.id = servicio.IdSolicitud')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->where('servicio.IdStatusServicio = :st ', array(':st' => $st))

                             ->orderBy('Id ASC')
                             ->limit($l)
                             ->offset($p)
                             ->fetchAll();//para mas de un registro

            $total = $this->db->from($this->table)
                              ->select('COUNT(*) Total')
                              ->leftJoin('solicitud ON solicitud.id = servicio.IdSolicitud')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->where('servicio.IdStatusServicio = :st', array(':st' => $st))
                              ->fetch()
                              ->Total;

            // $total->getQuery();
            $this->response->result = [
                'data'  => $data,
                'total' => $total
            ];
            return $this->response->SetResponse(true);

        }else{//dos estados 0 (solicitud) y 1 (cotizada)
          $data = $this->db  ->from($this->table)
                             ->select('solicitud.Id as IdSolicitud,servicio.Id as Id, solicitud.Descripcion,fotosolicitud.NombreArchivo as Foto,solicitud.RequiereVisita, solicitud.Sos,IFNULL(servicio.IdStatusServicio,solicitud.StatusSolicitud) AS IdStatusServicio, cotizacion.Id AS IdCotizacion')
                             ->rightJoin('solicitud ON servicio.IdSolicitud = solicitud.Id')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->leftJoin('cotizacion on  IdSolicitudServicio = solicitud.Id')
                             ->where('servicio.IdSolicitud is NULL')
                             ->where('solicitud.StatusSolicitud',$st)
                             ->limit($l)
                             ->offset($p)
                             ->orderBy('solicitud.Id DESC')
                             ->fetchAll();//para mas de un registro

            $total = $this->db->from($this->table)
                              ->select('COUNT(*) Total')
                              ->rightJoin('solicitud ON servicio.IdSolicitud = solicitud.Id')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->where('servicio.IdSolicitud is NULL ')
                             ->where('solicitud.StatusSolicitud',$st)
                              ->fetch()
                              ->Total;
            if($st == 2){
                $estado = 'Espera';
                $this->response->result = [
                    'data'  => $data,
                    'total' => $total,
                    'Estado' => $estado
                ];
                return $this->response->SetResponse(true);
            }
            if($st == 0){
                $estado = 'Solicitud';
                $this->response->result = [
                    'data'  => $data,
                    'total' => $total,
                    'Estado' => $estado
                ];
                return $this->response->SetResponse(true);
            }
            // $total->getQuery();
            $this->response->result = [
                'data'  => $data,
                'total' => $total
            ];
            return $this->response->SetResponse(true);
        }
    }
    #servicio para admin web del estado 2 al 4
    public function listarServicios($l,$p)
    {
      $data = $this->db  ->from($this->table)
                             ->select('servicio.Id, solicitud.Descripcion,fotosolicitud.NombreArchivo as Foto, solicitud.RequiereVisita, solicitud.Sos, solicitud.IdTipoServicio,tiposervicio.Descripcion AS TipoServicio,subtiposervicio.Descripcion AS SubtipoServicio,CONCAT(direccion.Calle," ",direccion.NoExt," ",direccion.NoInt," ",direccion.CP," ",direccion.Colonia," ",IFNULL(direccion.MunicipioDelegacion,"")," ",direccion.Ciudad," ",direccion.Estado) AS DireccionServicio,solicitud.IdCliente,CONCAT(persona.Nombre," ",persona.ApellidoPaterno) AS NombreCliente,solicitud.FechaDeseadaServicio AS FechaDeseada, solicitud.IdEspecialistaAsignado AS IdEspecialista,CONCAT(personaE.Nombre, " ",personaE.ApellidoPaterno) AS NombreEspecialista')
                             ->leftJoin('solicitud ON solicitud.id = servicio.IdSolicitud')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->leftJoin('tiposervicio ON solicitud.IdTipoServicio = tiposervicio.Id')
                             ->leftJoin('subtiposervicio ON solicitud.IdSubTipoServicio = subtiposervicio.Id')
                             ->leftJoin('direccion ON solicitud.IdDireccionServicio = direccion.Id')
                             ->leftJoin('persona ON solicitud.IdCliente = persona.Id')
                             ->leftJoin('persona AS personaE ON solicitud.IdEspecialistaAsignado = personaE.Id')
                             ->where('servicio.IdStatusServicio >= 2')
                             ->where('servicio.IdStatusServicio < 6')
                             ->orderBy('servicio.Id DESC')
                             ->limit($l)
                             ->offset($p)
                             ->fetchAll();//para mas de un registro
        $total = $this->db  ->from($this->table)
                             ->select('COUNT(*) Total')
                             ->leftJoin('solicitud ON solicitud.id = servicio.IdSolicitud')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->leftJoin('tiposervicio ON solicitud.IdTipoServicio = tiposervicio.Id')
                             ->leftJoin('subtiposervicio ON solicitud.IdSubTipoServicio = subtiposervicio.Id')
                             ->leftJoin('direccion ON solicitud.IdDireccionServicio = direccion.Id')
                             ->leftJoin('persona ON solicitud.IdCliente = persona.Id')
                             ->leftJoin('persona AS personaE ON solicitud.IdEspecialistaAsignado = personaE.Id')
                             ->where('servicio.IdStatusServicio >= 2')
                             ->where('servicio.IdStatusServicio < 6')
                             ->fetch()
                             ->Total;

                 $this->response->result =[ 
                      "data" => $data,
                      "total" => $total
                 ];            
          return $this->response->SetResponse(true);
    }
    #historial de servicios
    public function historial($u, $st)
    {
      if ($st > 2) {
          $data = $this->db  ->from($this->table)
                             ->select('servicio.Id, solicitud.Descripcion,fotosolicitud.NombreArchivo as Foto, solicitud.RequiereVisita, solicitud.Sos, solicitud.IdTipoServicio,tiposervicio.Descripcion AS TipoServicio,subtiposervicio.Descripcion AS SubtipoServicio,CONCAT(direccion.Calle," ",direccion.NoExt," ",direccion.NoInt," ",direccion.CP," ",direccion.Colonia," ",IFNULL(direccion.MunicipioDelegacion,"")," ",direccion.Ciudad," ",direccion.Estado) AS DireccionServicio,solicitud.IdCliente,CONCAT(persona.Nombre," ",persona.ApellidoPaterno) AS NombreCliente,solicitud.FechaDeseadaServicio AS FechaDeseada, solicitud.IdEspecialistaAsignado AS IdEspecialista,CONCAT(personaE.Nombre, " ",personaE.ApellidoPaterno) AS NombreEspecialista')

                             ->leftJoin('solicitud ON solicitud.id = servicio.IdSolicitud')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->leftJoin('tiposervicio ON solicitud.IdTipoServicio = tiposervicio.Id')
                             ->leftJoin('subtiposervicio ON solicitud.IdSubTipoServicio = subtiposervicio.Id')
                             ->leftJoin('direccion ON solicitud.IdDireccionServicio = direccion.Id')
                             ->leftJoin('persona ON solicitud.IdCliente = persona.Id')
                             ->leftJoin('persona AS personaE ON solicitud.IdEspecialistaAsignado = personaE.Id')
                             ->where('servicio.IdStatusServicio = :st AND (IdCliente = :u OR IdEspecialistaAsignado = :u)', array(':st' => $st , ':u' => $u))
                             ->orderBy('Id DESC')
                             ->fetchAll();//para mas de un registro

            $total = $this->db->from($this->table)
                              ->select('COUNT(*) Total')
                              ->leftJoin('solicitud ON solicitud.id = servicio.IdSolicitud')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->where('servicio.IdStatusServicio = :st AND (IdCliente = :u OR IdEspecialistaAsignado = :u)', array(':st' => $st , ':u' => $u))
                              ->fetch()
                              ->Total;

            // $total->getQuery();
            $this->response->result = [
                'data'  => $data,
                'total' => $total
            ];
            return $this->response->SetResponse(true);
        }else{ #agregar los dos mas 
          $data = $this->db  ->from($this->table)
                             ->select('solicitud.Id as IdSolicitud,servicio.Id, solicitud.Descripcion,fotosolicitud.NombreArchivo as Foto, solicitud.RequiereVisita, solicitud.Sos, IFNULL(servicio.IdStatusServicio,solicitud.StatusSolicitud) AS IdStatusServicio')
                             ->rightJoin('solicitud ON servicio.IdSolicitud = solicitud.Id')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->where('servicio.IdSolicitud is NULL AND solicitud.IdCliente = :u AND solicitud.StatusSolicitud = :st ', array(':u' => $u,':st' => $st))
                             ->orderBy('Id DESC')
                             ->fetchAll();//para mas de un registro

            $total = $this->db->from($this->table)
                              ->select('COUNT(*) Total')
                              ->rightJoin('solicitud ON servicio.IdSolicitud = solicitud.Id')
                             ->leftJoin('fotosolicitud ON solicitud.id = fotosolicitud.IdSolicitud')
                             ->where('servicio.IdSolicitud is NULL AND solicitud.IdCliente = :u AND solicitud.StatusSolicitud = :st ', array(':u' => $u,':st' => $st))
                              ->fetch()
                              ->Total;

            // $total->getQuery();
            $this->response->result = [
                'data'  => $data,
                'total' => $total
            ];
            return $this->response->SetResponse(true);
        }
    }
    public function obtener($id)
    {
        $buscar =  $this->db->from($this->table,$id)
                            ->select('solicitud.Descripcion AS Descripcion,fotosolicitud.NombreArchivo AS Foto,solicitud.RequiereVisita,solicitud.SOS,solicitud.IdTipoServicio,tiposervicio.Descripcion AS TipoServicio,subtiposervicio.Descripcion AS SubTipoServicio,CONCAT(direccion.Calle," ",direccion.NoExt," ",direccion.NoInt," ",direccion.CP," ",direccion.Colonia," ",IFNULL(direccion.MunicipioDelegacion,"")," ",direccion.Ciudad," ",direccion.Estado) AS DireccionServicio,solicitud.IdCliente,CONCAT(persona.Nombre," ",persona.ApellidoPaterno) AS NombreCliente,solicitud.FechaDeseadaServicio,solicitud.IdEspecialistaAsignado,CONCAT(personaE.Nombre," ",personaE.ApellidoPaterno) AS NombreEspecialista')

                            ->leftJoin('solicitud ON solicitud.id = servicio.IdSolicitud')
                            ->leftJoin('tiposervicio ON solicitud.IdTipoServicio = tiposervicio.Id')
                            ->leftJoin('subtiposervicio ON solicitud.IdSubTipoServicio = subtiposervicio.Id')
                            ->leftJoin('persona ON solicitud.IdCliente = persona.Id')
                            ->leftJoin('direccion ON solicitud.IdDireccionServicio = direccion.Id')
                            ->leftJoin('fotosolicitud ON solicitud.Id = fotosolicitud.IdSolicitud')
                            ->leftJoin('persona As personaE ON personaE.Id = solicitud.IdEspecialistaAsignado')

                    ->fetch();//para un solo dato o linea

        if ($buscar != false) {
            $this->response->result = $buscar;            
            return $this->response->SetResponse(true);

         }else{
            $this->response->errors[]='La cotizacion no existe';
            return $this->response->SetResponse(false);
         }

    }

    public function registrar($data)
    {
      $insertar = array(
        'IdSolicitud'=>$data["IdSolicitud"],
        'IdCotizacion'=>$data["IdCotizacion"],
        'FechaPlanInicio'=>$data["FechaPlanInicio"],
        'FechaPlanFin'=>$data["FechapPlanFin"],
        'CostoReal'=>$data["CostoReal"],
        'IdStatusServicio'=>3
      );
        $insertarServicio = $this->db->insertInto($this->table, $insertar)
                 ->execute();

        $actualizarSolicitud = $this->db->update($this->tableSec,array('IdEspecialistaAsignado' => $data['IdEspecialistaAsignado']), $data['IdSolicitud'])
                       ->execute();


            $new_data = array('IdServicio' => $insertarServicio);
            $data = array_merge($data, $new_data);
//         SELECT token.Token FROM `solicitud` 
// LEFT JOIN token ON token.IdUsuario = solicitud.IdEspecialistaAsignado
// WHERE solicitud.IdEspecialistaAsignado = 7 LIMIT 1
                       $token = $this->db->from($this->tableSec,$id)
                                ->leftJoin('token ON token.IdUsuario = solicitud.IdEspecialistaAsignado')
                                ->select('token.Token as Token')
                                ->where('solicitud.IdEspecialistaAsignado = :IdEspecialistaAsignado',array(':IdEspecialistaAsignado' => $data['IdEspecialistaAsignado']))
                                ->limit(1)
                                ->fetch()
                                ->Token;
                      $mensaje = "";
                      $Push = Push::FMC('Solicitud',$mensaje,$token,$data);
                      $token = $this->db->from($this->tableSec,$id)
                                ->leftJoin('token ON token.IdUsuario = solicitud.IdEspecialistaAsignado')
                                ->select('token.Token as Token')
                                ->where('solicitud.IdCliente = :IdCliente',array(':IdCliente' => $data['IdCliente']))
                                ->limit(1)
                                ->fetch()
                                ->Token;
                      $Push = Push::FMC('Solicitud',$mensaje,$token,$data);        
               $this->response->result=$insertarServicio;
        return $this->response->SetResponse(true);

    }
    public function encaminoEspecialista($id)
    {
      # enviar push a cliente y cambiar estado de servicio
      $actualizar= $this->db->update($this->table,array('IdStatusServicio'=> '4' ), $id)
                       ->execute();
      $token = $this->db->from($this->table)
                           ->leftJoin('solicitud ON solicitud.Id = servicio.IdSolicitud')
                           ->leftJoin('token on solicitud.IdCliente = token.IdUsuario')
                           ->where('servicio.Id',$id)
                           ->select('token.Token')
                           ->fetch()
                           ->Token;
                           $data = array('id' => $id );
              $Push = Push::FMC('Especialista en Camino','Especialista en Camino',$token,$data);
              $this->response->result = $id;
              return $this->response->SetResponse(true);


    }
    
    public function llegueEspecialista($id)
    {
      $token = $this->db->from($this->table)
                           ->leftJoin('solicitud ON solicitud.Id = servicio.IdSolicitud')
                           ->leftJoin('token on solicitud.IdCliente = token.IdUsuario')
                           ->where('servicio.Id',$id)
                           ->select('token.Token')
                           ->fetch()
                           ->Token;
                           $data = array('id' => $id );
              $Push = Push::FMC('Especialista esta en el lugar','El especialista esta en la ubicacion del servicio',$token,$data);
              $this->response->result = $id;
              return $this->response->SetResponse(true);
    }

    public function validarEspecialista($token,$id)
    {
            $token = $token['token'];
            $data = $this->db->from($this->table)
                           ->leftJoin('solicitud ON servicio.IdSolicitud = solicitud.Id')
                           ->leftJoin('codigoEspecialista ON codigoEspecialista.IdPersona = solicitud.IdEspecialistaAsignado')
                           ->where('servicio.Id',$id)
                           ->where('codigoEspecialista.Codigo',$token)
                           ->select('COUNT(codigoEspecialista.Codigo) as Token ')
                           ->fetch()
                           ->Token;

            if ($data == 0) {
                $this->response->errors[]="Token incorrecto";
                return $this->response->SetResponse(false);
            }else{

                $actualizar= $this->db->update($this->table,array('IdStatusServicio'=> '5' ), $id)
                       ->execute();
                $token = $this->db->from($this->table)
                           ->leftJoin('solicitud ON solicitud.Id = servicio.IdSolicitud')
                           ->leftJoin('token on solicitud.IdEspecialistaAsignado = token.IdUsuario')
                           ->where('servicio.Id',$id)
                           ->select('token.Token')
                           ->fetch()
                           ->Token;
                           $d = array('id' => $id );
                $Push = Push::FMC('Inicio de servicio','La infomacion del especialista es correcta',$token,$d);
                $info = $this->db->from($this->table,$id)
                                 ->select('persona.Id AS IdEspecialista, CONCAT(persona.Nombre," ",persona.ApellidoPaterno," ",persona.ApellidoMaterno) AS Nombre,persona.TelefonoCelular,persona.email,persona.FotoPerfil,persona.TipoPersona')
                                 ->leftJoin('solicitud ON servicio.IdSolicitud = solicitud.Id')
                                 ->leftJoin('persona ON persona.Id = solicitud.IdEspecialistaAsignado')
                                 ->fetch();
                                 $salida = 
                                 [
                                    "Id"=>$info->IdEspecialista,
                                    "Nombre"=>$info->Nombre,
                                    "TelefonoCelular"=>$info->TelefonoCelular,
                                    "email"=>$info->email,
                                    "FotoPerfil"=>$info->FotoPerfil,
                                    "TipoPersona"=>$info->TipoPersona
                                 ];
                $this->response->result=$salida;
                return $this->response->SetResponse(true);
            }
           
        #return $data;
    
    }

    public function finalizarServicio($token,$id)
    {
        $token = $token['token'];
            $data = $this->db->from($this->table)
                           ->leftJoin('solicitud ON servicio.IdSolicitud = solicitud.Id')
                           ->leftJoin('codigoEspecialista ON codigoEspecialista.IdPersona = solicitud.IdEspecialistaAsignado')
                           ->where('servicio.Id',$id)
                           ->where('codigoEspecialista.Codigo',$token)
                           ->select('COUNT(codigoEspecialista.Codigo) as Token ')
                           ->fetch()
                           ->Token;

            if ($data == 0) {
                $this->response->errors[]="Token incorrecto";
                return $this->response->SetResponse(false);
            }else{

                $actualizar= $this->db->update($this->table,array('IdStatusServicio'=> '6' ), $id)
                       ->execute();
                $token = $this->db->from($this->table)
                           ->leftJoin('solicitud ON solicitud.Id = servicio.IdSolicitud')
                           ->leftJoin('token on solicitud.IdEspecialistaAsignado = token.IdUsuario')
                           ->where('servicio.Id',$id)
                           ->select('token.Token')
                           ->fetch()
                           ->Token;
                           $d = array('id' => $id );
                $Push = Push::FMC('Inicio de servicio','La infomacion del especialista es correcta',$token,$d);
                $info = $this->db->from($this->table,$id)
                                 ->select('persona.Id AS IdEspecialista, CONCAT(persona.Nombre," ",persona.ApellidoPaterno," ",persona.ApellidoMaterno) AS Nombre,persona.TelefonoCelular,persona.email,persona.FotoPerfil,persona.TipoPersona')
                                 ->leftJoin('solicitud ON servicio.IdSolicitud = solicitud.Id')
                                 ->leftJoin('persona ON persona.Id = solicitud.IdEspecialistaAsignado')
                                 ->fetch();
                                 $salida = 
                                 [
                                    "Id"=>$info->IdEspecialista,
                                    "Nombre"=>$info->Nombre,
                                    "TelefonoCelular"=>$info->TelefonoCelular,
                                    "email"=>$info->email,
                                    "FotoPerfil"=>$info->FotoPerfil,
                                    "TipoPersona"=>$info->TipoPersona
                                 ];
                $this->response->result=$salida;
                return $this->response->SetResponse(true,'Trabajo finalizado');
            }
           
    }
    
    public function calificarServicio($data,$id)
    {
      $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {
            $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
          }else{
            $this->response->errors[]='La cotizacion no existe';
          return $this->response->SetResponse(false);
          }
    }
    public function actualizar($data,$id)
    {
       $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {
            $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
          }else{
            $this->response->errors[]='La cotizacion no existe';
          return $this->response->SetResponse(false);
          }
    }

    public function eliminar($id)
    {
         $eliminar = $this->db->deleteFrom($this->table,$id)
                 ->execute();
                 $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>