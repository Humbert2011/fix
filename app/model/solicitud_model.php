<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Push;

class SolicitudModel
{
    private $db;
    private $table = 'solicitud';
    private $tableSec = 'persona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    #lsitar solicitudes
    public function listar()
    {
    	 $data = $this->db->from($this->table)
                         ->orderBy('Id ASC')
                         ->fetchAll();

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
    #obtener solicitudes
    public function obtener($id)
    {
        $buscar =  $this->db->from($this->table,$id)
                            ->select('servicio.Id as IdServicio,solicitud.IdCliente AS IdPersona,CONCAT(persona.Nombre," ",persona.ApellidoPaterno) AS NombreCliente,CONCAT(direccion.Calle," ",direccion.NoExt," ",direccion.NoInt," ",direccion.CP," ",direccion.Colonia," ",IFNULL(direccion.MunicipioDelegacion,"")," ",direccion.Ciudad," ",direccion.Estado) AS DireccionServicio,direccion.Longitud,direccion.Latitud,tiposervicio.Descripcion AS TipoServicio,IFNULL(subtiposervicio.Descripcion,0) AS SubTipoServicio,IFNULL(servicio.IdStatusServicio,solicitud.StatusSolicitud) AS IdStatusServicio')
                            ->leftJoin('persona ON solicitud.IdCliente = persona.Id')
                            ->leftJoin('direccion ON solicitud.IdDireccionServicio = direccion.Id')
                            ->leftJoin('tiposervicio ON solicitud.IdTipoServicio = tiposervicio.Id')
                            ->leftJoin('subtiposervicio ON solicitud.IdSubTipoServicio = subtiposervicio.Id')
                            ->leftJoin('servicio ON solicitud.Id = servicio.IdSolicitud')
                            //->leftJoin('cotizacion ON  solicitud.Id = cotizacion.IdSolicitudServicio')
                            // ->where('cotizacion.Aprobada = 1 or cotizacion.Aprobada is NULL')
                    ->fetch();
        $idCotizacion = $this->db->from('cotizacion')
                                ->where('IdSolicitudServicio',$id)
                                //->where('cotizacion.Aprobada = 1')
                                ->fetch()
                                ->Id;

        $data = get_object_vars($buscar); //convertir objeto en arreglo
        $new_data = array('idCotizacion' => $idCotizacion);
        $data = array_merge($data, $new_data);
        if ($buscar != false) {
            $this->response->result = $data;
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='La solicitud no se encuentra';
            return $this->response->SetResponse(false);
         }
    }
    #alta de solicitud
    public function registrar($data)
    {
        $mensaje = $data['Descripcion'].' - '.$data['FechaDeseadaServicio'];
        $insertarSolicitud = $this->db->insertInto($this->table, $data)
                 ->execute();
        $buscarAdmin = $this->db
                         ->from($this->tableSec)
                         ->leftJoin('token ON persona.Id = token.IdUsuario')
                         ->select('Token')
                         ->where('Activo = 1 AND TipoPersona = "Admin"') 
                         ->limit(1)
                         ->fetch('Token');
                         $data = array('id' => $insertarSolicitud ); 
        $Push = Push::FMC('Solicitud',$mensaje,$buscarAdmin,$data);
               $this->response->result= $insertarSolicitud;
        return $this->response->SetResponse(true);
    }
     #actualizar solicitud
    public function actualizar($data,$id)
    {
        $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {
            $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
          }else{
          	$this->response->errors[]='La solicitud no existe';
          return $this->response->SetResponse(false);
          }
    }
    #eliminar solicitud
    public function eliminar($id)
    {
        $eliminar = $this->db->deleteFrom($this->table,$id)
                 ->execute();
                 $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
 }
?>