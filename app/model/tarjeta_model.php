<?php
namespace App\Model;

require_once __DIR__ . '/../lib/conekta-php-master/lib/Conekta.php';
\Conekta\Conekta::setApiKey("key_Kohz8SEzi2zJfud7GE8VVQ");
\Conekta\Conekta::setApiVersion("2.0.0");

use App\Lib\Response,
	App\Lib\MontoCk;

class TarjetaModel
{
	private $db;
    private $table = 'tarjeta';
    private $tablePersona = 'persona';
    private $tableTransaccion = 'transaccion';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    #registro de tarjetas y de usuario
    public function registrar($tokenCard,$idCliente)
    {
    	$buscarUser =  $this->db->from($this->tablePersona)
    				->where('Id',$idCliente)
                    ->fetch();

        if ($buscarUser != false) {
        	$buscarTar =  $this->db->from($this->table)
    				->where('idCliente',$idCliente)
                    ->fetch();
            if ($buscarTar != false) {
            	#agregar tarjeta al usuario
            	// return "#agregar tarjeta al usuario";
            	$idCustomer = $buscarTar->IdClienteConekta;
            	//return $idCustomer;
            	$customer = \Conekta\Customer::find($idCustomer);
				$source = $customer->createPaymentSource(array(
				  	"type" => "card",
					"token_id" => $tokenCard
				));
				//verificar existencia de tarjeta
	            #variables de validacion
				$last4 = $source->last4;
				$bin = $source->bin;
				$exp_month  = $source->exp_month;
				$exp_year = $source->exp_year;
				$brand = $source->brand;
				$max = count($customer->payment_sources);
				$i = 0;
				$dupli =0 ;
				while ( $i< $max) {
					$sourceActu = $customer->payment_sources[$i];
					#
					$last4Actu = $sourceActu->last4;
					$binActu = $sourceActu->bin;
					$exp_monthActu  = $sourceActu->exp_month;
					$exp_yearActu = $sourceActu->exp_year;
					$brandActu = $sourceActu->brand;
					#
					if(($last4 == $last4Actu )AND($bin == $binActu)AND($brand == $brandActu)){
						$dupli = $dupli + 1;
					}
					$i++;
				}
				if ($dupli < 2) {
					//return '{"success":1, "content":['.$source.']}';
					$this->response->result=$source;
                	return $this->response->SetResponse(true,"Metodo de pago dado de alta");
				}else{
					$source->delete();
					//return '{"success":0,"error_message":"Esta tarjeta ya se encuentra registrada como método de pago."}';
					$this->response->errors[]='Esta tarjeta ya se encuentra registrada como método de pago.';
            		return $this->response->SetResponse(false); 

				}
            }else{
            	#dar de alta al usuario y a la tarjeta
            	//return "#dar de alta al usuario y a la tarjeta";
            	$Nombre = $buscarUser->Nombre.' '.$buscarUser->ApellidoPaterno.' '.$buscarUser->ApellidoMaterno;
            	$email = $buscarUser->email;
            	$Telefono = $buscarUser->TelefonoCelular;
            	//return $Nombre.' '.$email.' '.$Telefono;
            	try {
				  	$customer = \Conekta\Customer::create(
				    array(
				      "name" => $Nombre,
				      "email" => $email,
				      "phone" => $Telefono,
				      "payment_sources" => array(
				        array(
				            "type" => "card",
				            "token_id" => $tokenCard
				       		 )
					      )//payment_sources
					    )
				    	//customer
				  	);
				    //echo $customer->id." id customer";
				    $idCustomer = $customer->id; //id del cliente
				    $customer = \Conekta\Customer::find($idCustomer);
				    $source = $customer->payment_sources[0];
				    //$idCard = $source->id;
				    $data = ['IdCliente'	=> $idCliente,
				    		 'IdClienteConekta'	=>$idCustomer
				    		 ];
					if ($idCustomer!="") {
				    	$altaCustomer = $this->db->insertInto($this->table, $data)
                     							 ->execute();
                     	$this->response->result=['idCustomer'=>$altaCustomer,
                     							 'paymentSources'=>$source];
                		return $this->response->SetResponse(true,"Metodo de pago dado de alta");
				    }else{
				    	$this->response->errors[]='Error al encontrar a cliente';
            			return $this->response->SetResponse(false);
				    }
				} catch (\Conekta\ProccessingError $error){
				  //echo $error->getMesage();
					$this->response->errors[]=$error->getMessage();
            		return $this->response->SetResponse(false);
				} catch (\Conekta\ParameterValidationError $error){
				  // echo $error->getMessage();
					$this->response->errors[]=$error->getMessage();
            		return $this->response->SetResponse(false);
				} catch (\Conekta\Handler $error){
				  // echo $error->getMessage();
					$this->response->errors[]=$error->getMessage();
            		return $this->response->SetResponse(false);
				}
            }
        }else{
        	$this->response->errors[]='El Cliente no se encuntra';
            return $this->response->SetResponse(false); 
        }
    }
    #listar tarjetas del cliente
    public function listar($idCliente)
    {
        $buscarUser = $this->db->from($this->table)
    				->where('idCliente',$idCliente)
                    ->fetch();
        if ($buscarUser != false) {
        	$idCustomer = $buscarUser->IdClienteConekta;
	        $customer = \Conekta\Customer::find($idCustomer);
				//echo $customer->payment_sources;//informacion del metodo de pago
			$max = count($customer->payment_sources);
	        if ($max > 0) {
	        			//$result = json_encode($customer->payment_sources);
	        			$result = json_decode($customer->payment_sources);
	        	   	   $this->response->result = $result;
	        	return $this->response->SetResponse(true);
	        }else{
	        		   $this->response->errors[] = 'No hay metodos de pago';
	        	return $this->response->SetResponse(false);
	        }
        }else{
        	$this->response->errors[] = 'No hay tarjetas para este cliente';
	        return $this->response->SetResponse(false);
        }
        
    }
    #obtener informacion de tarjeta
    public function obtener($idCliente,$idTarjeta)
    {
    	$buscarUser = $this->db->from($this->table)
    				->where('idCliente',$idCliente)
                    ->fetch();
        if ($buscarUser != false) {
        	$idCustomer = $buscarUser->IdClienteConekta;
	        $customer = \Conekta\Customer::find($idCustomer);
	        $count = count($customer->payment_sources);
	        for ($i=0; $i < $count; $i++) { 
	        	if ($idTarjeta === $customer->payment_sources[$i]->id) {
	        		$tar = $customer->payment_sources[$i];
	        	}
	        }
	        if ($tar != null) {
	        	$this->response->result = $tar;
	        	return $this->response->SetResponse(true);
	        }else{
	        	$this->response->errors[] = 'Tarjeta no encontrada';
	        	return $this->response->SetResponse(false);	
	        }
        		
        }else{
        	$this->response->errors[] = 'No hay tarjetas para este cliente';
	        return $this->response->SetResponse(false);
        }
    }
    #actualizar tarjeta de cliente
    public function actualizar($data,$idCliente,$idTarjeta)
    {
		$nombre = $data['nombre'];
		$mes = $data['mes'];
		$year = $data['year'];

    	$buscarUser = $this->db->from($this->table)
    				->where('idCliente',$idCliente)
                    ->fetch();
        if ($buscarUser != false) {
        	$idCustomer = $buscarUser->IdClienteConekta;
	        $customer = \Conekta\Customer::find($idCustomer);
	        $count = count($customer->payment_sources);
	        for ($i=0; $i < $count; $i++) { 
	        	if ($idTarjeta === $customer->payment_sources[$i]->id) {
	        		$customer->payment_sources[$i]->update(array(
						'name'=>$nombre,
						'exp_month'=>$mes,
						'exp_year'=>$year
					));
	        		$tar = $customer->payment_sources[$i];
	        	}
	        }
        		if ($tar != null) {
	        	$this->response->result = $tar;
	        	return $this->response->SetResponse(true);
		        }else{
		        	$this->response->errors[] = 'Tarjeta no encontrada';
		        return $this->response->SetResponse(false);	
		        }

        }else{
        	$this->response->errors[] = 'No hay tarjetas para este cliente';
	        return $this->response->SetResponse(false);
        }
    }
    #eliminar tarjeta de cliente
    public function eliminar($idCliente,$idTarjeta)
    {
    	$buscarUser = $this->db->from($this->table)
    				->where('idCliente',$idCliente)
                    ->fetch();
        if ($buscarUser != false) {
        	$idCustomer = $buscarUser->IdClienteConekta;
	        $customer = \Conekta\Customer::find($idCustomer);
	        $count = count($customer->payment_sources);
	        for ($i=0; $i < $count; $i++) { 
	        	if ($idTarjeta === $customer->payment_sources[$i]->id) {
	        		$customer->payment_sources[$i]->delete();
	        		$tar = $customer->payment_sources[$i];
	        	}
	        }
        		if ($tar != null) {
	        	$this->response->result = $tar;
	        	return $this->response->SetResponse(true);
		        }else{
		        	$this->response->errors[] = 'Tarjeta no encontrada';
		        return $this->response->SetResponse(false);	
		        }

        }else{
        	$this->response->errors[] = 'No hay tarjetas para este cliente';
	        return $this->response->SetResponse(false);
        }
    }
    #metodo de pago por default
    public function defaultPaymentSource($idCliente,$idTarjeta)
    {
    	$buscarUser = $this->db->from($this->table)
    				->where('idCliente',$idCliente)
                    ->fetch();
        if ($buscarUser != false) {
        	$idCustomer = $buscarUser->IdClienteConekta;
	        $customer = \Conekta\Customer::find($idCustomer);
	        $count = count($customer->payment_sources);
	        for ($i=0; $i < $count; $i++) { 
	        	if ($idTarjeta === $customer->payment_sources[$i]->id) {
	        		#$customer->payment_sources[$i]->delete();
	        		$tar = $customer->payment_sources[$i];
	        		$customer->update(array(default_payment_source_id => $tar->id));
	        		$lugar = $i;
	        	}
	        }
        		if ($tar != null) {
	        	$this->response->result = $customer->payment_sources[$lugar];
	        	return $this->response->SetResponse(true);
		        }else{
		        	$this->response->errors[] = 'Tarjeta no encontrada';
		        return $this->response->SetResponse(false);	
		        }

        }else{
        	$this->response->errors[] = 'No hay tarjetas para este cliente';
	        return $this->response->SetResponse(false);
        }
    }
    #orden de cobro de servicio
    public function orden($data)
    {

    	$monto = $data['monto'];
    	$idCliente = $data['id'];
    	$idTarjeta = $data['idTar'];
    	$idServicio = $data['idServicio'];
    	#
    	$buscarUser = $this->db->from($this->table)
    				->where('idCliente',$idCliente)
                    ->fetch();
        if ($buscarUser != false) {
        	$idCustomer = $buscarUser->IdClienteConekta;
        	//return $idCustomer;
	    	$montoMascomision = MontoCk::calcular($monto);
		   	$montoConekta = $montoMascomision * 100;
			$montoMsjConekta = $montoMascomision/100;

			//return $montoMascomision;
	    	# datos tabla  transaccion Id	IdTarjeta	IdServicio	Monto	IdTransaccionConekta	Status	FechaPeticion
	    	try{
			  $order = \Conekta\Order::create(
			    array(
			      "line_items" => array(
			        array(
			          "name" => "Saldo Pikr $".$montoMsjConekta,
			          "unit_price" => $montoConekta,
			          "quantity" => 1
			        )//first line_item
			      ), //line_items
			      "shipping_lines" => array(
			        array(
			          "amount" => 0,
			           "carrier" => "null"
			        )
			      ), //shipping_lines - physical goods only
			      "currency" => "MXN",
			      "customer_info" => array(
			        "customer_id" => $idCustomer
			      ), //customer_info
			      "shipping_contact" => array(
		       	  "address" => array(
		          "street1" => "Calle null",
		          "postal_code" => "00000",
		          "country" => "MX"
		        )//address
		      ), //shipping_contact - required only for physical goods
			      //"metadata" => array("reference" => "12987324097", "more_info" => "lalalalala"),
			      "charges" => array(
			          array(
			              "payment_method" => array(
			                      "type" => "card",
		     					  "payment_source_id" => $idTarjeta
			              ) //payment_method - use customer's <code>default</code> - a card
			          ) //first charge
			      ) //charges
			    )//order
			  );

			  #guardar en base de datos IdServicio	Monto	IdTransaccionConekta	Status	FechaPeticion
			  $array = array('IdTarjeta	' => $idTarjeta,
			  				 'IdServicio' => $idServicio,
			  				 'Monto' 	  => $montoMascomision,
			  				 'IdTransaccionConekta' => $order->id,
			  				 'Status' 	  => $order->payment_status
			  				);
			  #$idOrden = $order->id;
			  #$status = $order->payment_status

			  	$insertarTran = $this->db->insertInto($this->tableTransaccion, $array)
                 					  ->execute();
             	$this->response->result=$insertarTran;
        		return $this->response->SetResponse(true);
			  #
			} catch (\Conekta\ProccessingError $error){
				 //echo $error->getMesage();
				$this->response->errors[]=$error->getMessage();
	           	return $this->response->SetResponse(false);
			} catch (\Conekta\ParameterValidationError $error){
				// echo $error->getMessage();
				$this->response->errors[]=$error->getMessage();
	           	return $this->response->SetResponse(false);
			} catch (\Conekta\Handler $error){
				// echo $error->getMessage();
				$this->response->errors[]=$error->getMessage();
	           	return $this->response->SetResponse(false);
			}
		#final de orden de cobro
		}else{
        	$this->response->errors[] = 'No hay tarjetas para este cliente';
	        return $this->response->SetResponse(false);
        }
    }

}



?>