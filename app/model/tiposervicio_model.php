<?php
namespace App\Model;

use App\Lib\Response;

class TiposervicioModel
{
    private $db;
    private $table = 'tiposervicio';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    #lsitar tiposervicio
    public function listar()
    {
        $data = $this->db
                         ->from($this->table)
                         ->leftJoin('fototiposervicio ON fototiposervicio.IdTipoServicio = tiposervicio.Id')
                         ->select('fototiposervicio.NombreArchivo as Foto')
                         ->orderBy('Id DESC')
                         ->fetchAll();

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
    public function listarsubservicios($s)
    {
       $data = $this->db
                         ->from($this->table)
                         ->leftJoin('subtiposervicio ON subtiposervicio.IdTipoServicio = tiposervicio.Id')
                         ->select('subtiposervicio.Id as Idsub, subtiposervicio.Descripcion,subtiposervicio.VisitaObligatoria')
                         ->where('tiposervicio.Id',$s)
                         ->orderBy('Id DESC')
                         ->fetchAll();

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->leftJoin('subtiposervicio ON subtiposervicio.IdTipoServicio = tiposervicio.Id')
                           ->select('COUNT(*) Total')
                           ->where('tiposervicio.Id',$s)
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
    #obtener tipodireccion
    public function obtener($id)
    {

        $buscar =  $this->db->from($this->table,$id)
                    ->fetch();

        if ($buscar != false) {
            $this->response->result = $buscar;
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='El tipo de servicio no existe';
            return $this->response->SetResponse(false);
         }

    }
    #alta de tipodireccion
    public function registrar($data)
    {
        $insertartipodireccion = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result=$insertartipodireccion;
        return $this->response->SetResponse(true);
    }
    #actualizar tipodireccion
    public function actualizar($data,$id)
    {
        $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {
            $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
          }else{
            $this->response->errors[]='El tipo de servicio no existe';
          return $this->response->SetResponse(false);
          }
    }
    #eliminar
    public function eliminar($id)
    {
        $eliminar = $this->db->deleteFrom($this->table,$id)
                 ->execute();
                 $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>