<?php
namespace App\Model;

use App\Lib\Response;

class TokenModel
{
    private $db;
    private $table = 'token';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    #lsitar token
    public function listar($l, $p , $user )
    {
        $data = $this->db->from($this->table)
                         ->where('IdUsuario',$user)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('Id DESC')
                         ->fetchAll();

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->where('IdUsuario',$user)
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
    // #obtener token
    // public function obtener($id)
    // {

    //     $buscar =  $this->db->from($this->table,$id)
    //                 ->fetch();

    //     if ($buscar != false) {
    //         $this->response->result = $buscar;
    //         return $this->response->SetResponse(true);
    //      }else{
    //         $this->response->errors[]='El token no se encuentra';
    //         return $this->response->SetResponse(false);
    //      }

    // }
    #alta de token
    public function registrar($data)
    {
        $IdUsuario = $data['IdUsuario'];
        $buscar =  $this->db->from($this->table)
                    ->where('IdUsuario',$IdUsuario)
                    ->fetch();
        if ($buscar != false) {
             $actualizar= $this->db->update($this->table, $data)
                       ->where('IdUsuario',$IdUsuario)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
         }else{
            $insertartoken = $this->db->insertInto($this->table, $data)
                 ->execute();
                $this->response->result=$insertartoken;
                return $this->response->SetResponse(true);
         }
       
    
    }
    #actualizar token
    public function actualizar($data,$id)
    {
        $buscar = $this->db->from($this->table,$id)
                      ->select('COUNT(*) Num')
                      ->fetch()
                      ->Num;
          if ($buscar > 0) {
            $actualizar= $this->db->update($this->table, $data, $id)
                       ->execute();
              $this->response->result = $actualizar;
              return $this->response->SetResponse(true);
          }else{
            $this->response->errors[]='El token no existe';
          return $this->response->SetResponse(false);
          }
    }
    #eliminar
    public function eliminar($id)
    {
        $eliminar = $this->db->deleteFrom($this->table,$id)
                 ->execute();
                 $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>