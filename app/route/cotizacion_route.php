<?php
use App\Lib\login,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/cotizacion/', function () {
    //listar
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->cotizacion->listar())
                   );
    });
    //listarCotizaciones
    $this->get('listarCotizaciones/{u}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->cotizacion->listarCotizaciones($args['u']))
                   );
    });
    //
    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cotizacion->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cotizacion->registrar($req->getParsedBody()))
                 );
    });

    $this->put('rechazar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cotizacion->rechazar($req->getParsedBody(),$args['id']))
                 );
    });

    $this->put('aceptar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cotizacion->aceptar($args['id']))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cotizacion->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cotizacion->eliminar($args['id']))
                 );
    });
});