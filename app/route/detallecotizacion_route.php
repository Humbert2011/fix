<?php
use App\Lib\login,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/detallecotizacion/', function () {
    $this->get('listar/{l}/{p}/{u}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->detallecotizacion->listar($args['l'],$args['p'],$args['u']))
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detallecotizacion->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detallecotizacion->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detallecotizacion->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->detallecotizacion->eliminar($args['id']))
                 );
    });
});