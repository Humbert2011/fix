<?php
use App\Lib\login,
    App\Lib\Response,
    App\Validation\DireccionValidation,
    App\Middleware\AuthMiddleware;

$app->group('/direccion/', function () {
    $this->get('listar/{u}', function ($req, $res, $args) {
      $r = DireccionValidation::dir_listar($args);
         if(!$r->response){
             return $res->withHeader('Content-type', 'application/json')
                      ->withStatus(422)
                      ->write(json_encode($r));
        }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->direccion->listar($args['u']))
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      $r = DireccionValidation::dir_obtener($args);
         if(!$r->response){
             return $res->withHeader('Content-type', 'application/json')
                      ->withStatus(422)
                      ->write(json_encode($r));
        }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->direccion->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      $r = DireccionValidation::dir_registrar($req->getParsedBody());
      if(!$r->response){
        return $res->withHeader('Content-type', 'application/json')
                 ->withStatus(422)
                 ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->direccion->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {
      $r = DireccionValidation::dir_actualizar($req->getParsedBody());
      if(!$r->response){
        return $res->withHeader('Content-type', 'application/json')
                 ->withStatus(422)
                 ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->direccion->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->put('defecto/{id}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->direccion->defecto($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      $r = DireccionValidation::dir_eliminar($args);
         if(!$r->response){
             return $res->withHeader('Content-type', 'application/json')
                      ->withStatus(422)
                      ->write(json_encode($r));
        }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->direccion->eliminar($args['id']))
                 );
    });
});