<?php 
use App\Lib\Login,
	App\Lib\Response,
	App\Validation\EspecialistaValidation,
	App\Middleware\AuthMiddleware;

	$app->group('/especialista/',function(){
		$this->get('obtener/{id}', function($req, $res, $args){
			return $res->withHeader('Content-type','application/json')
                   ->write(
                   json_encode($this->model->especialista->obtener($args['id']))
                 );
		});

		$this->get('listar/{l}/{p}', function($req, $res, $args){
			$r = EspecialistaValidation::esp_listar($args);
	         if(!$r->response){
	             return $res->withHeader('Content-type', 'application/json')
	                      ->withStatus(422)
	                      ->write(json_encode($r));
	        }
			return $res->withHeader('Content-type','application/json')
					->write(
						json_encode($this->model->especialista->listar($args['l'],$args['p']))
					);
		});

		$this->get('listars', function($req, $res, $args){
			return $res->withHeader('Content-type','application/json')
					->write(
						json_encode($this->model->especialista->listars())
					);
		});

		$this->post('registrar',function($req, $res, $args){
			$r = EspecialistaValidation::esp_registrar($req->getParsedBody());
	        if(!$r->response){
	          return $res->withHeader('Content-type', 'application/json')
	                   ->withStatus(422)
	                   ->write(json_encode($r));
	        }
			return $res->withHeader('Content-type','application/json')
					   ->write(
					   	json_encode($this->model->especialista->registrar($req->getParsedBody(),'Especialista'))
					   );
		});

		$this->put('actualizar/{id}', function ($req, $res, $args){
			$r = EspecialistaValidation::esp_actualizar($req->getParsedBody());
	        if(!$r->response){
	          return $res->withHeader('Content-type', 'application/json')
	                   ->withStatus(422)
	                   ->write(json_encode($r));
	        }
			 return $res->withHeader('Content-type','application/json')
                 	->write(
                   	json_encode($this->model->especialista->actualizar($req->getParsedBody(), $args['id']))
                 	   );
        });

		$this->delete('eliminar/{id}', function ($req, $res, $args) {
	      return $res->withHeader('Content-type','application/json')
	                 ->write(
	                   json_encode($this->model->persona->eliminar($args['id']))
	                 );
	    });
	});

 ?>