<?php
use App\Lib\Login,
    App\Lib\Response,
    App\Validation\LoginValidation,
    App\Middleware\AuthMiddleware;

$app->group('/login/', function () {
    #autenticar al usuario
    $this->post('autenticar', function ($req, $res, $args) {
        $r = LoginValidation::login($req->getParsedBody());
        if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
        }
        $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->login->autenticar($parametros['TelefonoCelular'],$parametros['Password'],$parametros['TipoPersona']))
        );
    });    

    #validar numero
    $this->post('validarNumero', function ($req, $res, $args) {
      $r = LoginValidation::validarNum($req->getParsedBody());
        if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->login->validarNumero($parametros['CodigoPais'],$parametros['TelefonoCelular'])
        ));
    });

    #validar codigo
    $this->post('validarCodigo', function ($req, $res, $args){
      $r = LoginValidation::validarCod($req->getParsedBody());
        if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
              ->write(
                json_encode($this->model->login->validarCodigo($parametros['TelefonoCelular'],$parametros['Codigo'])
              ));
    });

    $this->get('validaUser/{id}', function ($req, $res, $args){
       return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->login->validaUser($args['id']))
                   );
    });

    $this->get('validaC/{id}/{c}', function ($req, $res, $args){
      $r = LoginValidation::validarC($args);
        if(!$r->response){
            return $res->withHeader('Content-type', 'application/json')
                       ->withStatus(422)
                       ->write(json_encode($r));
      }
       return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->login->validaC($args['id'],$args['c']))
                   );
    });

    $this->put('ActualizaPass/{id}', function($req, $res, $args){
      $r = LoginValidation::actualiza_Pass($req->getParsedBody());
        if(!$r->response){
            return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
                 ->write(
                    json_encode($this->model->login->ActualizaPass($args['id'],$req->getParsedBody())
                  ));
    });

    #recuperar password
    $this->post('recuperarPassword', function ($req, $res, $args) {
        $r = LoginValidation::recuperarPass($req->getParsedBody());
        if(!$r->response){
            return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
        }
        $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
                     ->write(
                      json_encode($this->model->login->recuperarPassword($parametros['email'])
                      )
        );
    });

    #Token
    $this->get('getData/{token}', function ($req, $res, $args) {
      $r = LoginValidation::gettoken($args);
        if(!$r->response){
            return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
        }
        return $res->withHeader('Content-type','application/json')
                     ->write(
                     json_encode($this->model->login->getData($args['token']))
                     );
    });
});
