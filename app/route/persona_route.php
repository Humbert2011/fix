<?php
use App\Lib\login,
    App\Lib\Response,
    App\Validation\PersonaValidation,
    App\Middleware\AuthMiddleware;

$app->group('/persona/', function () {
    $this->get('listar/{l}/{p}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->persona->listar($args['l'],$args['p']))
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->obtener($args['id']))
                 );
    });
    //usuario
    $this->post('registrar', function ($req, $res, $args) {
      $r = PersonaValidation::validate($req->getParsedBody());
      if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->registrar($req->getParsedBody(),"Usuario"))
                 );
    });
    //profecional
    $this->post('registrarE', function ($req, $res, $args) {
      $r = PersonaValidation::validate($req->getParsedBody());
      if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->registrar($req->getParsedBody(),"Especialista"))
                 );
    });
    //Admin
    $this->post('registrarA', function ($req, $res, $args) {
      $r = PersonaValidation::validate($req->getParsedBody());
      if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->registrar($req->getParsedBody(),"Admin"))
                 );
    });
    //
     $this->post('regFacebook', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->regFacebook($req->getParsedBody(),"Usuario"))
                 );
      });
    //
    $this->put('actualizar/{id}', function ($req, $res, $args) {
      $r = PersonaValidation::validate($req->getParsedBody(),true);
      if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->eliminar($args['id']))
                 );
    });
});
#->add(new AuthMiddleware($app))
