<?php
use App\Lib\login,
    App\Lib\Response,
    App\Validation\ServicioValidation,
    App\Middleware\AuthMiddleware;

$app->group('/servicio/', function () {
  
  $this->get('listaEspecialista/{id}', function ($req, $res, $args) {
         $r = ServicioValidation::serv_listaEsp($args);
         if(!$r->response){
             return $res->withHeader('Content-type', 'application/json')
                      ->withStatus(422)
                      ->write(json_encode($r));
        }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->servicio->listaEspecialista($args['id']))
                   );
    });

    $this->get('listar/{l}/{p}/{st}', function ($req, $res, $args) {
         $r = ServicioValidation::serv_listar($args);
         if(!$r->response){
             return $res->withHeader('Content-type', 'application/json')
                      ->withStatus(422)
                      ->write(json_encode($r));
        }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->servicio->listar($args['l'],$args['p'],$args['st']))
                   );
    });
    //listar servicios para web del estado 1 al 3
    $this->get('listarServicios/{l}/{p}', function ($req, $res, $args) {
         $r = ServicioValidation::serv_listarServicios($args);
         if(!$r->response){
             return $res->withHeader('Content-type', 'application/json')
                      ->withStatus(422)
                      ->write(json_encode($r));
        }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->servicio->listarServicios($args['l'],$args['p']))
                   );
    });
    //
    $this->get('historial/{u}/{st}', function ($req, $res, $args) {
        $r = ServicioValidation::serv_historial($args);
         if(!$r->response){
             return $res->withHeader('Content-type', 'application/json')
                      ->withStatus(422)
                      ->write(json_encode($r));
        }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->servicio->historial($args['u'],$args['st']))
                   );
    });
    //
    $this->get('obtener/{id}', function ($req, $res, $args) {
      $r = ServicioValidation::serv_obtener($args);
         if(!$r->response){
             return $res->withHeader('Content-type', 'application/json')
                      ->withStatus(422)
                      ->write(json_encode($r));
        }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->servicio->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      $r = ServicioValidation::serv_registrar($req->getParsedBody());
      if(!$r->response){
        return $res->withHeader('Content-type', 'application/json')
                 ->withStatus(422)
                 ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->servicio->registrar($req->getParsedBody()))
                 );
    });

    $this->put('encaminoEspecialista/{id}', function ($req, $res, $args) {      
      $r = ServicioValidation::serv_encaminoEspecialista($args);
         if(!$r->response){
            return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->servicio->encaminoEspecialista($args['id']))
                 );
    });

    $this->put('llegueEspecialista/{id}', function ($req, $res, $args) {
      $r = ServicioValidation::serv_llegueEspecialista($args);
         if(!$r->response){
            return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->servicio->llegueEspecialista($args['id']))
                 );
    });

    $this->put('validarEspecialista/{id}', function ($req, $res, $args) {
      $tok = $req->getParsedBody();
      $data = array(
                    'id'=>$args['id'],
                    'token'=>$tok['token']);
      $r = ServicioValidation::serv_validarEspecialista($data);
      if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                   ->withStatus(422)
                   ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->servicio->validarEspecialista($req->getParsedBody(),$args['id']))
                 );
    });

    $this->put('finalizarServicio/{id}', function ($req, $res, $args) {
      $tok = $req->getParsedBody();
      $data = array(
                    'id'=>$args['id'],
                    'token'=>$tok['token']);
      $r = ServicioValidation::serv_finalizarServicio($data);
      if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                   ->withStatus(422)
                   ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->servicio->finalizarServicio($req->getParsedBody(),$args['id']))
                 );
    });

    $this->put('calificarServicio/{id}', function ($req, $res, $args) {
      $cal = $req->getParsedBody();
      $data = array(
                    'id'=>$args['id'],
                    'Calificacion'=>$cal['Calificacion']);
      $r = ServicioValidation::serv_calificarServicio($data);
      if(!$r->response){
          return $res->withHeader('Content-type', 'application/json')
                   ->withStatus(422)
                   ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->servicio->calificarServicio($req->getParsedBody(), $args['id']))
                 );
    });
    $this->put('actualizar/{id}', function ($req, $res, $args) {
      $r = ServicioValidation::serv_actualizar($args);
         if(!$r->response){
            return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->servicio->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      $r = ServicioValidation::serv_eliminar($args);
         if(!$r->response){
            return $res->withHeader('Content-type', 'application/json')
                     ->withStatus(422)
                     ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->servicio->eliminar($args['id']))
                 );
    });
});