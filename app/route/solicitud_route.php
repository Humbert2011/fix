<?php
use App\Lib\login,
    App\Lib\Response,
    App\Validation\SolicitudValidation,
    App\Middleware\AuthMiddleware;

$app->group('/solicitud/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->solicitud->listar())
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      $r = SolicitudValidation::sol_obtener($args);
       if(!$r->response){
           return $res->withHeader('Content-type', 'application/json')
                    ->withStatus(422)
                    ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->solicitud->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      $r = SolicitudValidation::sol_registrar($req->getParsedBody());
      if(!$r->response){
        return $res->withHeader('Content-type', 'application/json')
                 ->withStatus(422)
                 ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->solicitud->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {
      $r = SolicitudValidation::sol_actualizar($req->getParsedBody());
      if(!$r->response){
        return $res->withHeader('Content-type', 'application/json')
                 ->withStatus(422)
                 ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->solicitud->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      $r = SolicitudValidation::sol_eliminar($args);
       if(!$r->response){
           return $res->withHeader('Content-type', 'application/json')
                    ->withStatus(422)
                    ->write(json_encode($r));
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->solicitud->eliminar($args['id']))
                 );
    });
});