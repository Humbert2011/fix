<?php
use App\Lib\login,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/tarjeta/', function () {
    $this->get('listar/{l}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->tarjeta->listar($args['l']))
                   );
    });

    $this->get('obtener/{id}/{idTar}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarjeta->obtener($args['id'],$args['idTar']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarjeta->registrar($parametros['token_card'],$parametros['id']))
                 );
    });

    $this->put('actualizar/{id}/{idTar}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarjeta->actualizar($req->getParsedBody(), $args['id'], $args['idTar']))
                 );
    });

    $this->delete('eliminar/{id}/{idTar}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarjeta->eliminar($args['id'],$args['idTar']))
                 );
    });

    $this->put('defaultPaymentSource', function ($req, $res, $args) {
       $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarjeta->defaultPaymentSource($parametros['id'],$parametros['idTar']))
                 );
    });
    
    $this->post('orden', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarjeta->orden($req->getParsedBody()))
                 );
    });

    
});
?>