<?php
use App\Lib\login,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/tiposervicio/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->tiposervicio->listar())
                   );
    });
    #listarsubservicios
    //
    $this->get('listarsubservicios/{s}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->tiposervicio->listarsubservicios($args['s']))
                   );
    });
    //
    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tiposervicio->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tiposervicio->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tiposervicio->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tiposervicio->eliminar($args['id']))
                 );
    });
});