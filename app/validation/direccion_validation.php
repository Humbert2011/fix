<?php
	namespace App\Validation;
	use App\Lib\Response;

	class DireccionValidation{
		public static function dir_listar($data){
			$response = new Response();
			
			$key = 'u';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function dir_obtener($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function dir_registrar($data){
			$response = new Response();

			$key = 'TipoDireccion';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 50) {
					$response->errors[$key] = 'Este campo debe contener 50 caracteres o menos';
				}
			}

			$key = 'IdPropietario';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El Id tiene que ser un numero';
				}
			}
			
			$key = 'Calle';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 100) {
					$response->errors[$key] = 'Este campo debe contener 100 caracteres o menos';
				}
			}

			$key = 'NoExt';
			$value = $data[$key];
			if (strlen($value) >= 5) {
				$response->errors[$key] = 'Este campo debe contener 5 caracteres o menos';
			} 

			$key = 'NoInt';
			$value = $data[$key];
			if (strlen($value) >= 5) {
				$response->errors[$key] = 'Este campo debe contener 5 caracteres o menos';
			}

			$key = 'CP';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'Este cammpo solo adminte numeros';
				}
			}

			$key = 'Colonia';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 45) {
					$response->errors[$key] = 'Este campo debe contener 45 caracteres o menos';
				}
			}

			$key = 'MunicipioDelegacion';
			$value = $data[$key];
			if (strlen($value) >= 45) { 
				$response->errors[$key] = 'Este campo debe contener 45 caracteres o menos';
			}
			
			$key = 'Ciudad';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 45) {
					$response->errors[$key] = 'Este campo debe contener 45 caracteres o menos';
				}
			}

			$key = 'Estado';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 45) {
					$response->errors[$key] = 'Este campo debe contener 45 caracteres o menos';
				}
			}
			$key = 'ReferenciaAdicional';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 250) {
					$response->errors[$key] = 'Este campo debe contener 250 caracteres o menos';
				}
			}

			$key = 'Longitud';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'Este cammpo solo adminte numeros';
				}
				if (strlen($value) > 10) {
					$response->errors[$key] = 'Este campo solo admite 25 caracteres o menos';
				}
				
			}
			
			$key = 'Latitud';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'Este cammpo solo adminte numeros';
				}
				if (strlen($value) > 10) {
					$response->errors[$key] = 'Este campo solo admite 25 caracteres o menos';
				}
			}

			$key = 'Guardada';
			$value = $data[$key];
			if ($value != 'true' && $value != 'false' && $value != "") {
				$response->errors[$key] = 'El valor ingresado no es válido';
			}
			
			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function dir_actualizar($data){
			$response = new Response();

			$key = 'TipoDireccion';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 50) {
					$response->errors[$key] = 'Este campo debe contener 50 caracteres o menos';
				}
			}

			$key = 'IdPropietario';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El Id tiene que ser un numero';
				}
			}
			
			$key = 'Calle';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 100) {
					$response->errors[$key] = 'Este campo debe contener 100 caracteres o menos';
				}
			}

			$key = 'NoExt';
			$value = $data[$key];
			if (strlen($value) >= 5) {
				$response->errors[$key] = 'Este campo debe contener 5 caracteres o menos';
			} 

			$key = 'NoInt';
			$value = $data[$key];
			if (strlen($value) >= 5) {
				$response->errors[$key] = 'Este campo debe contener 5 caracteres o menos';
			}

			$key = 'CP';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'Este cammpo solo adminte numeros';
				}
			}

			$key = 'Colonia';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 45) {
					$response->errors[$key] = 'Este campo debe contener 45 caracteres o menos';
				}
			}

			$key = 'MunicipioDelegacion';
			$value = $data[$key];
			if (strlen($value) >= 45) { 
				$response->errors[$key] = 'Este campo debe contener 45 caracteres o menos';
			}
			
			$key = 'Ciudad';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 45) {
					$response->errors[$key] = 'Este campo debe contener 45 caracteres o menos';
				}
			}

			$key = 'Estado';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 45) {
					$response->errors[$key] = 'Este campo debe contener 45 caracteres o menos';
				}
			}
			$key = 'ReferenciaAdicional';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 250) {
					$response->errors[$key] = 'Este campo debe contener 250 caracteres o menos';
				}
			}

			$key = 'Longitud';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'Este cammpo solo adminte numeros';
				}
				if (strlen($value) > 10) {
					$response->errors[$key] = 'Este campo solo admite 10 caracteres o menos';
				}
				
			}
			
			$key = 'Latitud';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es necesario';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'Este cammpo solo adminte numeros';
				}
				if (strlen($value) > 10) {
					$response->errors[$key] = 'Este campo solo admite 10 caracteres o menos';
				}
			}

			$key = 'Guardada';
			$value = $data[$key];
			if ($value != 'true' && $value != 'false' && $value != "") {
				$response->errors[$key] = 'El valor ingresado no es válido';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function dir_eliminar($data){
			$response = new Response();

			$key = 'id';
			if (!is_numeric($data[$key])) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}
	}

 ?>