<?php 
namespace App\Validation;
use App\Lib\Response;

class EspecialistaValidation{
	public static function esp_listar($data){
		$response = new Response();

		$key = 'l';
		if (!isset($data[$key])) {
			$response->errors[$key] = 'Este campo es necesario';
		} else {
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El límite debe de ser un numero';
			}
			if ($value == 0) {
				$response->errors[$key] = 'El límite tiene que ser mayor a 0';
			}
		}

		$key = 'p';
		if (!isset($data[$key])) {
			$response->errors[$key] = 'Este campo es necesario';
		} else {
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'La página debe de ser un numero';
			}
		}

		$response->setResponse(count($response->errors) === 0);
        return $response;
	}

	public static function esp_registrar($data){
		$response = new Response();

		$key = 'Nombre';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo nombre es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) >= 60) {
				$response->errors[$key] = 'El campo nombre debe contener 60 caracteres o menos';
			}
		}

		$key = 'ApellidoPaterno';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo apellido paterno es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) >= 50) {
				$response->errors[$key] = 'El campo apellido paterno debe contener 50 caracteres o menos';
			}
		}

		$key = 'ApellidoMaterno';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo apellido materno es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) >= 50) {
				$response->errors[$key] = 'El campo apellido materno debe contener 50 caracteres o menos';
			}
		}

		$key = 'TelefonoCelular';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo telefono celular es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) < 6) {
				$response->errors[$key] = 'El campo telefono celular debe contener mínimo 6 caracteres';
			}
			if (!is_numeric($value)) {
			$response->errors[$key] = 'El campo telefono celular solo admite números';
			}
		}
		
		$key = 'email';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo email es obligatorio';
		} else {
			$value = $data[$key];
			if (!filter_var($value,FILTER_VALIDATE_EMAIL)) {
				$response->errors[$key] = 'El campo email solo admite un correo';
			}
		}
		
		$key = 'Password';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo password es obligatorio';
		} else {
			$value = $data[$key];
			if (strlen($value) < 6) {
				$response->errors[$key] = 'El campo password debe contener mínimo 6 caracteres';
			}
		}
		
		$key = 'TelefonoCasa';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo telefono de casa es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) < 6) {
				$response->errors[$key] = 'El campo telefono casa debe contener mínimo 6 caracteres';
			}
			if (!is_numeric($value)) {
			$response->errors[$key] = 'El campo telefono casa solo admite números';
			}
		}

		$key = 'Activo';
		if (empty($data[$key]) && !isset($data[$key])) {
			$response->errors[$key] = 'El campo activo es necesario';
		} else {
			$value = $data[$key];			
			if (!is_numeric($value)) {
			$response->errors[$key] = 'El campo activo solo admite números';
			}
			if ($value != 0 && $value != 1) {
				$response->errors[$key] = 'Valor ingresado en "activo" no válido';
			}
		}

		$key = 'FotoPerfil';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo foto de perfil es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) >= 150) {
				$response->errors[$key] = 'El campo foto de perfil debe contener mínimo 150 caracteres';
			}
		}

		$response->setResponse(count($response->errors) === 0);
        return $response;
	}

	public static function esp_actualizar($data){
		$response = new Response();

		$key = 'Nombre';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo nombre es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) >= 60) {
				$response->errors[$key] = 'El campo nombre debe contener 60 caracteres o menos';
			}
		}

		$key = 'ApellidoPaterno';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo apellido paterno es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) >= 50) {
				$response->errors[$key] = 'El campo apellido paterno debe contener 50 caracteres o menos';
			}
		}

		$key = 'ApellidoMaterno';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo apellido materno es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) >= 50) {
				$response->errors[$key] = 'El campo apellido materno debe contener 50 caracteres o menos';
			}
		}

		$key = 'TelefonoCelular';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo telefono celular es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) < 10 || strlen($value) > 10) {
				$response->errors[$key] = 'El campo telefono celular debe contener 10 caracteres';
			}
			if (!is_numeric($value)) {
			$response->errors[$key] = 'El campo telefono celular solo admite números';
			}
		}
		
		$key = 'email';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo email es obligatorio';
		} else {
			$value = $data[$key];
			if (!filter_var($value,FILTER_VALIDATE_EMAIL)) {
				$response->errors[$key] = 'El campo email solo admite un correo';
			}
		}
		
		$key = 'TelefonoCasa';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo telefono de casa es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) < 6) {
				$response->errors[$key] = 'El campo telefono casa debe contener mínimo 6 caracteres';
			}
			if (!is_numeric($value)) {
			$response->errors[$key] = 'El campo telefono casa solo admite números';
			}
		}

		$key = 'Activo';
		if (empty($data[$key]) && !isset($data[$key])) {
			$response->errors[$key] = 'El campo activo es necesario';
		} else {
			$value = $data[$key];			
			if (!is_numeric($value)) {
			$response->errors[$key] = 'El campo "activo" solo admite números';
			}
			if ($value != 0 && $value != 1) {
				$response->errors[$key] = 'Valor ingresado en "activo" no válido';
			}
		}

		$key = 'FotoPerfil';
		if (empty($data[$key])) {
			$response->errors[$key] = 'El campo foto de perfil es necesario';
		} else {
			$value = $data[$key];
			if (strlen($value) >= 150) {
				$response->errors[$key] = 'El campo foto de perfil debe contener mínimo 150 caracteres';
			}
		}

		$response->setResponse(count($response->errors) === 0);
        return $response;
	}
}

 ?>