<?php 
	namespace App\Validation;

	use App\Lib\Response;

	class LoginValidation{
		public static function login($data){
			$response = new Response();

			$key = 'TelefonoCelular';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			}else{
				$value = $data[$key];
				//Tamaño de la cadena a evaluar menor a 10
				if (strlen($value < 10)) {
					$response->errors[$key] = 'Debe contener 10 caracteres y ser un número';
				}
			}

			$key = 'Password';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			}else{
				$value = $data[$key];
				if (strlen($value < 6)) {
					$response->errors[$key] = 'Debe contener como minimo 6 caracteres';
				}
			}

			$key = 'TipoPersona';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			}else{
				$value = $data[$key];
				if ($value!='Usuario') {
					if ($value!='Admin') {
						if ($value!='Especialista') {
							$response->errors[$key] = 'No es un tipo válido';							
						}
					}					
				}
				
			}
			//Errores
			$response->setResponse(count($response->errors) === 0);
        	return $response;
		}

		public static function recuperarPass($data){
			$response = new Response();
			$key = 'email';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!filter_var($value,FILTER_VALIDATE_EMAIL)) {
					$response->errors[$key] = 'El formato ingresado no es un Email';
				}
			}
			//Errores
			$response->setResponse(count($response->errors) === 0);
			return $response;
		}

		public static function validarNum($data){
			$response = new Response();
			$key = 'CodigoPais';
			if(empty($data[$key])) {
            	$response->errors[$key] = 'Este campo es obligatorio';
        	} else {
            	$value = $data[$key];

            	if(strlen($value) < 2) {
                	$response->errors[$key] = 'Debe contener minimo 2 caracteres y ser un numero';
            	}
        	}

			$key = 'TelefonoCelular';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if(strlen($value) < 10) {
                $response->errors[$key] = 'Debe contener 10 caracteres y ser un numero';
            	}
			}
			//Errores
			$response->setResponse(count($response->errors) === 0);
        	return $response;
		}

		public static function validarCod($data){
			$response = new Response();
			$key = 'TelefonoCelular';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if(strlen($value) < 10){
					$response->errors[$key] = 'Debe contener 10 caracteres y ser un numero';
				}
			}
			
			$key = 'Codigo';
			if(empty($data[$key])) {
            	$response->errors[$key] = 'Este campo es obligatorio';
        	} else {
            	$value = $data[$key];

            	if(strlen($value) < 4) {
                	$response->errors[$key] = 'Debe contener 4 caracteres y ser un numero';
            	}
        	}
        	//Errores
        	$response->setResponse(count($response->errors) === 0);
        	return $response;
		}

		public static function validarC($data){
			$response = new Response();

			$key = 'c';
			if(empty($data[$key])){
				$response->errors[$key] = 'El codigo es necesario';
			}else{
				$value = $data[$key];
				if(strlen($value) < 6){
                	$response->errors[$key] = 'El codigo debe contener 6 caracteres como minimo';
            	}
			}
            $response->setResponse(count($response->errors) === 0);
        	return $response;
		}

		public static function actualiza_Pass($data){
			$response = new Response();

			$key = 'Password';
			if(empty($data[$key])){
				$response->errors[$key] = 'El Password es obligatorio';
			}else{
				$value = $data[$key];
				if(strlen($value) < 6){
                	$response->errors[$key] = 'El Password debe contener 6 caracteres como minimo';
            	}
			}
            $response->setResponse(count($response->errors) === 0);
        	return $response;
		}

		public static function gettoken($data){
			$response = new Response();
			$key = 'token';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			}else{
				$value = $data[$key];
				if (strlen($value) < 100) {
					$response->errors[$key] = 'Debe contener mas de 100 caracteres';
					if (strlen($value > 300)) {
						$response->errors[$key] = 'Debe contener menos de 300 caracteres';
					}
				}
			}

			$response->setResponse(count($response->errors) === 0);
			return $response;
		}
	}
 ?>