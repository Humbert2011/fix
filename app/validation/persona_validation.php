<?php
namespace App\Validation;

use App\Lib\Response;

class PersonaValidation {
    public static function validate($data, $update = false) {
        $response = new Response();

        $key = 'Nombre';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];
            if(strlen($value) >= 60) {
                $response->errors[$key] = 'Debe contener menos de 60 caracteres';
            }
        }
        $key = 'ApellidoPaterno';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];
            if(strlen($value) >= 50) {
                $response->errors[$key] = 'Debe contener menos de 50 caracteres';
            }
        }

        $key = 'ApellidoMaterno';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];
            if(strlen($value) >= 50) {
                $response->errors[$key] = 'Debe contener menos de 50 caracteres';
            }
        }

        $key = 'TelefonoCelular';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];
            if(strlen($value) < 10) {
                $response->errors[$key] = 'Debe contener 10 caracteres y ser un numero';
            }
        }

        $key = 'email';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];
            if(!filter_var($value,FILTER_VALIDATE_EMAIL)) {
                $response->errors[$key] = 'Valor ingresado no es un correo';
            }
        }

        $key = 'Password';
        if (!$update) {
          if(empty($data[$key])){
              $response->errors[$key] = 'Este campo es obligatorio';
          } else {
              $value = $data[$key];
              if(strlen($value) < 6) {
                  $response->errors[$key] = 'Debe contener como mínimo 6 caracteres';
              }
          }
        }else {
          if(!empty($data[$key])){
              $value = $data[$key];
              if(strlen($value) < 6) {
                  $response->errors[$key] = 'Debe contener como mínimo 6 caracteres';
              }
          }
        }

        $key = 'TelefonoCasa';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];
            if(strlen($value) < 10) {
                $response->errors[$key] = 'Debe contener 10 caracteres y ser un numero';
            }
        }

        $key = 'Activo';
        if(!isset($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];
            if($value != 1 && $value !=0) {
                $response->errors[$key] = 'Valor ingresado no valido';
            }
        }

        // $key = 'FotoPerfil';
        // if(!isset($data[$key])) {
        //     $response->errors[$key][] = 'Este campo es obligatorio';
        // } else {
        //     $value = $data[$key];

        //     if(strlen($value) >= 150) {
        //         $response->errors[$key][] = 'Supera el numero de caracteres reservado';
        //     }
        // }

        #cadena de errores
        $response->setResponse(count($response->errors) === 0);
        return $response;
    }
}
