<?php 
	namespace App\Validation;
	use App\Lib\Response;

	class ServicioValidation{
		public static function serv_listaEsp($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}
			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_listar($data){
			$response = new Response();

			$key = 'l';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'Tiene que ser un numero';
			}

			$key = 'p';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'Tiene que ser un numero';
			}

			$key = 'st';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'Tiene que ser un numero';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_listarServicios($data){
			$response = new Response();

			$key = 'l';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'Tiene que ser un numero';
			}

			$key = 'p';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'Tiene que ser un numero';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_historial($data){
			$response = new Response();

			$key = 'u';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'Tiene que ser un numero';
			}

			$key = 'st';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'Tiene que ser un numero';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_obtener($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_registrar($data){
			$response = new Response();

			$key = 'IdSolicitud';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El id tiene que ser un numero';
				}
			}

			$key = 'IdCotizacion';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El id tiene que ser un numero';
				}
			}

			$key = 'FechaPlanInicio';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!strtotime($value)) {
					$response->errors[$key] = 'El formato debe ser aaaa-mm-dd';
				}
			}
			
			$key = 'FechaPlanFin';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!strtotime($value)) {
					$response->errors[$key] = 'El formato debe ser aaaa-mm-dd';
				}
			}

			$key = 'CostoReal';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'Este campo solo acepta numeros';
				}
			}
			
			$key = 'IdEspecialistaAsignado';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El id tiene que ser un numero';
				}
			}
			
			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_encaminoEspecialista($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}
			
			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_llegueEspecialista($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}
			
			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_validarEspecialista($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}

			//FALTA ARREGLAR AQUÍ ********************************************************
			$key = 'token';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (strlen($value) < 20) {
					$response->errors[$key] = 'El token debe tener 20 o mas carcateres';
				}
			}			
			
			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_finalizarServicio($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}

			//FALTA ARREGLAR AQUÍ ********************************************************
			$key = 'token';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (strlen($value) < 20) {
					$response->errors[$key] = 'El token debe tener 20 o mas carcateres';
				}
			}			
			
			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_calificarServicio($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}

			$key = 'Calificacion';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'La calificacion debe de ser un numero';
			} 
			
			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_actualizar($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}

		public static function serv_eliminar($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El Id tiene que ser un numero';
			}

			$response->SetResponse(count($response->errors) === 0);
			return $response;
		}
	}

 ?>