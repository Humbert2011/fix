<?php 
	namespace App\Validation;
	use App\Lib\Response;

	class SolicitudValidation{
		public static function sol_obtener($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El id tiene que ser un numero';
			}
			
			$response->setResponse(count($response->errors) === 0);
			return $response;
		}

		public static function sol_registrar($data){
			$response = new Response();

			$key = 'Descripcion';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 250) {
					$response->errors[$key] = 'La descripcion debe tener 250 o menos carcateres';
				}
			}

			$key = 'FechaDeseadaServicio';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!strtotime($value)) {
					$response->errors[$key] = 'El formato debe ser aaaa-mm-dd';
				}
			}

			$key = 'ComentarioAdicional';
			$value = $data[$key];
			if (strlen($value) >= 250) {
				$response->errors[$key] = 'El comentario debe de tener 250 carcateres o menos';
			}			
			
			$key = 'IdCliente';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El id tiene que ser un numero';
				}
			}
			
			$key = 'IdDireccionServicio';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El id tiene que ser un numero';
				}
			}
			
			$key = 'IdTipoServicio';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El id tiene que ser un numero';
				}
			}
			
			$key = 'IdSubTipoServicio';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El id tiene que ser un numero';
				}
			}

			$key = 'RequiereVisita';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if($value != 1 && $value !=0) {
                	$response->errors[$key] = 'Valor ingresado no valido, debe ser 0 o 1';                	
            	}
            	if (!is_numeric($value)) {
               		$response->errors[$key] = 'El Id tiene de ser un numero';
                }
			}

			$key = 'Sos';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if($value != 1 && $value !=0) {
                	$response->errors[$key] = 'Valor ingresado no valido, debe ser 0 o 1';                	
            	}
            	if (!is_numeric($value)) {
               		$response->errors[$key] = 'El Id tiene de ser un numero';
                }
			}

			$response->setResponse(count($response->errors) === 0);
			return $response;
		}

		public static function sol_actualizar($data){
			$response = new Response();

			$key = 'Descripcion';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (strlen($value) >= 250) {
					$response->errors[$key] = 'La descripcion debe tener 250 o menos carcateres';
				}
			}

			$key = 'FechaSolicitud';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!strtotime($value)) {
					$response->errors[$key] = 'El formato debe ser aaaa-mm-dd';
				}
			}

			$key = 'FechaDeseadaServicio';
			if (empty($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!strtotime($value)) {
					$response->errors[$key] = 'El formato debe ser aaaa-mm-dd';
				}
			}

			$key = 'ComentarioAdicional';
			$value = $data[$key];
			if (strlen($value) >= 250) {
				$response->errors[$key] = 'el comentario tener 250 caracteres o menos';
			}

			$key = 'IdCliente';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El Id tiene que se un numero';
				} 				
			}

			$key = 'IdDireccionServicio';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El Id tiene que se un numero';
				} 				
			}
			
			$key = 'IdTipoServicio';
			if (!isset($data[$key])) {
				$response->errors[$key] = 'Este campo es obligatorio';
			} else {
				$value = $data[$key];
				if (!is_numeric($value)) {
					$response->errors[$key] = 'El Id tiene que ser un numero';
				}				
			}			

			$response->setResponse(count($response->errors) === 0);
			return $response;
		}

		public static function sol_eliminar($data){
			$response = new Response();

			$key = 'id';
			$value = $data[$key];
			if (!is_numeric($value)) {
				$response->errors[$key] = 'El id tiene que ser un numero';
			}

			$response->setResponse(count($response->errors) === 0);
			return $response;
		}
	}

 ?>