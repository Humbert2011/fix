-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-10-2018 a las 11:12:14
-- Versión del servidor: 5.7.23-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbfikz`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigoEspecialista`
--

CREATE TABLE `codigoEspecialista` (
  `Id` int(10) NOT NULL,
  `IdPersona` int(10) NOT NULL,
  `Codigo` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `codigoEspecialista`
--

INSERT INTO `codigoEspecialista` (`Id`, `IdPersona`, `Codigo`) VALUES
(1, 7, 'B1U1QxNRhGX6rcGS39Bk');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracionplataforma`
--

CREATE TABLE `configuracionplataforma` (
  `Id` int(10) UNSIGNED NOT NULL,
  `NombreVariable` varchar(45) DEFAULT NULL,
  `Descripcion` varchar(200) DEFAULT NULL,
  `Valor` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE `cotizacion` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdCreadaPor` int(11) NOT NULL,
  `IdSolicitudServicio` int(11) NOT NULL,
  `MontoTotal` double DEFAULT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Aprobada` tinyint(1) DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cotizacion`
--

INSERT INTO `cotizacion` (`Id`, `IdCreadaPor`, `IdSolicitudServicio`, `MontoTotal`, `Descripcion`, `Aprobada`, `FechaCreacion`) VALUES
(32, 1, 23, 1500, 'hacer camino', 1, '2018-10-05 11:22:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallecotizacion`
--

CREATE TABLE `detallecotizacion` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdCotizacion` int(10) UNSIGNED NOT NULL,
  `IdCreadoPor` int(11) NOT NULL,
  `FechaCreacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `Descripcion` varchar(400) DEFAULT NULL,
  `Monto` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleservicio`
--

CREATE TABLE `detalleservicio` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdCreadoPor` int(10) UNSIGNED NOT NULL,
  `Descripcion` varchar(400) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IdServicio` int(10) NOT NULL,
  `Monto` double DEFAULT NULL,
  `Tipo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion`
--

CREATE TABLE `direccion` (
  `Id` int(10) UNSIGNED NOT NULL,
  `TipoDireccion` varchar(50) NOT NULL,
  `IdPropietario` int(11) NOT NULL,
  `Calle` varchar(100) DEFAULT NULL,
  `NoExt` varchar(5) DEFAULT NULL,
  `NoInt` varchar(10) DEFAULT NULL,
  `CP` varchar(5) DEFAULT NULL,
  `Colonia` varchar(45) DEFAULT NULL,
  `MunicipioDelegacion` varchar(45) DEFAULT NULL,
  `Ciudad` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `EntreCalle1` varchar(100) DEFAULT NULL,
  `EntreCalle2` varchar(100) DEFAULT NULL,
  `ReferenciaAdicional` varchar(250) DEFAULT NULL,
  `Longitud` varchar(10) DEFAULT NULL,
  `Latitud` varchar(10) DEFAULT NULL,
  `Guardada` tinyint(1) NOT NULL,
  `Defecto` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `direccion`
--

INSERT INTO `direccion` (`Id`, `TipoDireccion`, `IdPropietario`, `Calle`, `NoExt`, `NoInt`, `CP`, `Colonia`, `MunicipioDelegacion`, `Ciudad`, `Estado`, `EntreCalle1`, `EntreCalle2`, `ReferenciaAdicional`, `Longitud`, `Latitud`, `Guardada`, `Defecto`) VALUES
(1, '1', 1, 'Benito Juarez', '19', '1', '73165', 'Patoltecoya', 'Huauchinango', 'Huauchinango', 'Puebla', 'V. Carranza', 'Nicolas Bravo', 'Casa con un barandal de patos', '20.2089823', '-98.033676', 0, 0),
(2, '1', 1, 'Corregidora', '45', '1', '73170', 'Centro', 'Huauchinango', 'Huauchinango', 'Puebla', 'Guerrero', 'Nicolas Bravo', 'Ultimo local de la plaza', '20.1731643', '-98.051984', 0, 0),
(3, '1', 1, 'Corregidora', '45', '1', '73170', 'Centro', 'Huauchinango', 'Huauchinango', 'Puebla', 'Guerrero', 'Nicolas Bravo', 'Ultimo local de la plaza', '20.1731643', '-98.051984', 0, 0),
(4, '1', 1, 'Corregidora', '45', '1', '73170', 'Centro', 'Huauchinango', 'Huauchinango', 'Puebla', NULL, NULL, 'Ultimo local de la plaza', '20.1731643', '-98.051984', 0, 0),
(5, 'Casa', 1, 'Corregidora', '45', '1', '73170', 'Centro', 'Huauchinango', 'Huauchinango', 'Puebla', NULL, NULL, 'Ultimo local de la plaza', '20.1731643', '-98.051984', 1, 1),
(6, 'Oficina', 1, 'Corregidora', '45', '1', '73170', 'Centro', 'Huauchinango', 'Huauchinango', 'Puebla', NULL, NULL, 'Ultimo local de la plaza', '20.1731643', '-98.051984', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialistaservicio`
--

CREATE TABLE `especialistaservicio` (
  `IdEspecialista` int(11) NOT NULL,
  `IdTipoServicio` int(10) UNSIGNED NOT NULL,
  `NivelDeConfianza` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadosservicio`
--

CREATE TABLE `estadosservicio` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdServicio` int(11) NOT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `FechaInicio` varchar(45) DEFAULT NULL,
  `FechaFin` varchar(45) DEFAULT NULL,
  `Comentarios` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotodetallecotizacion`
--

CREATE TABLE `fotodetallecotizacion` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdDetalleCotizacion` int(10) UNSIGNED NOT NULL,
  `FechaDeCarga` datetime DEFAULT NULL,
  `NombreArchivo` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fotodetallecotizacion`
--

INSERT INTO `fotodetallecotizacion` (`Id`, `IdDetalleCotizacion`, `FechaDeCarga`, `NombreArchivo`) VALUES
(3, 1, '2018-07-25 14:39:45', 'C:/AppServ/www/fix/img/cotizacion/cotizacion_1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotodetalleservicio`
--

CREATE TABLE `fotodetalleservicio` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdDetalleServicio` int(10) UNSIGNED NOT NULL,
  `FechaDeCarga` datetime DEFAULT NULL,
  `NombreArchivo` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotoincidencia`
--

CREATE TABLE `fotoincidencia` (
  `Id` int(11) NOT NULL,
  `FotoDetalleServicio_Id` int(10) UNSIGNED NOT NULL,
  `FechaDeCarga` datetime DEFAULT NULL,
  `NombreArchivo` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fotoincidencia`
--

INSERT INTO `fotoincidencia` (`Id`, `FotoDetalleServicio_Id`, `FechaDeCarga`, `NombreArchivo`) VALUES
(1, 1, '2018-08-02 11:56:24', 'C:/AppServ/www/fix/img/incidencia/incidencia_1.jpg'),
(2, 1, '2018-08-07 00:15:32', 'C:/AppServ/www/fix/img/incidencia/incidencia_1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotosolicitud`
--

CREATE TABLE `fotosolicitud` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdSolicitud` int(11) NOT NULL,
  `FechaDeCarga` datetime DEFAULT NULL,
  `NombreArchivo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fotosolicitud`
--

INSERT INTO `fotosolicitud` (`Id`, `IdSolicitud`, `FechaDeCarga`, `NombreArchivo`) VALUES
(2, 3, '2018-08-18 00:00:00', 'solicitud3'),
(3, 1, '2018-09-06 00:18:18', 'C:/AppServ/www/fix/img/solicitud/solicitud_1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fototiposervicio`
--

CREATE TABLE `fototiposervicio` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdTipoServicio` int(11) NOT NULL,
  `FechaDeCarga` datetime DEFAULT NULL,
  `NombreArchivo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fototiposervicio`
--

INSERT INTO `fototiposervicio` (`Id`, `IdTipoServicio`, `FechaDeCarga`, `NombreArchivo`) VALUES
(13, 1, '2018-08-22 11:15:18', 'C:/AppServ/www/fix/img/tiposervicio/tiposervicio_1.png'),
(14, 2, '2018-08-22 11:19:34', 'C:/AppServ/www/fix/img/tiposervicio/tiposervicio_2.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialservicio`
--

CREATE TABLE `historialservicio` (
  `Id` int(11) NOT NULL,
  `IdServicio` int(10) UNSIGNED NOT NULL,
  `IdStatusAnterior` int(11) NOT NULL,
  `IdStatusNuevo` int(11) NOT NULL,
  `ComentariosAdicionales` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidenciaservicio`
--

CREATE TABLE `incidenciaservicio` (
  `Id` int(11) UNSIGNED NOT NULL,
  `IdCreadaPor` varchar(45) DEFAULT NULL,
  `Descripcion` varchar(400) DEFAULT NULL,
  `IdServicio` int(10) UNSIGNED NOT NULL,
  `Fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `Monto` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `incidenciaservicio`
--

INSERT INTO `incidenciaservicio` (`Id`, `IdCreadaPor`, `Descripcion`, `IdServicio`, `Fecha`, `Monto`) VALUES
(1, '1', 'No hay material solo se solicito la mano de obra', 4, '2018-09-20 12:41:56', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdDestinatario` int(11) NOT NULL,
  `IdTipo` int(11) NOT NULL,
  `FechaEnvio` datetime DEFAULT NULL,
  `Mensaje` varchar(2000) DEFAULT NULL,
  `FechaExpiracion` datetime DEFAULT NULL,
  `Codigo` varchar(6) DEFAULT NULL,
  `TempTelefono` varchar(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(60) NOT NULL,
  `ApellidoPaterno` varchar(50) DEFAULT NULL,
  `ApellidoMaterno` varchar(50) DEFAULT NULL,
  `TelefonoCelular` varchar(10) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `TelefonoCasa` varchar(10) DEFAULT NULL,
  `Activo` tinyint(1) DEFAULT NULL,
  `FotoPerfil` varchar(150) DEFAULT NULL,
  `TipoPersona` varchar(45) DEFAULT NULL,
  `IdFacebook` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`Id`, `Nombre`, `ApellidoPaterno`, `ApellidoMaterno`, `TelefonoCelular`, `email`, `Password`, `TelefonoCasa`, `Activo`, `FotoPerfil`, `TipoPersona`, `IdFacebook`) VALUES
(1, 'Humberto', 'Martinez', 'Cuautenco', '7761052213', 'zion_leyenda@hotmail.com', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '7767625590', 1, 'http://fikz.stardust.com.mx/img/perfil/perfil_1.jpg', 'Admin', NULL),
(2, 'Elisa', 'Perez', 'Ortega', '7711787698', 'eli10915.ep@gmail.com', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '7767625590', 1, 'C:/AppServ/www/fix', 'Usuario', NULL),
(4, 'Humberto', 'Martinez', 'Cuautenco', '7761032969', 'martingb53@hotmail.com', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '7767625590', 1, NULL, 'Usuario', NULL),
(7, 'Humberto', 'Martinez', 'Cuautenco', '7761052214', 'martingb53@gmail.com', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '7767625590', 1, NULL, 'Especialista', NULL),
(8, 'Humberto', 'Martinez', 'Cuautenco', '7761052314', 'martingb53@yahoo.com', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '7767625590', 1, NULL, 'Admin', NULL),
(11, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Usuario', '165156165161561616156166666'),
(12, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Usuario', '1sdasda44as64as5d4a6sa6s46a4s65a4sf65a46f4a6sda6a5s65f6f6f6f66');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdSolicitud` int(11) NOT NULL,
  `IdCotizacion` int(10) UNSIGNED NOT NULL,
  `FechaPlanInicio` datetime DEFAULT NULL,
  `FechaPlanFin` datetime DEFAULT NULL,
  `CostoReal` double DEFAULT NULL,
  `Calificacion` tinyint(4) DEFAULT NULL,
  `IdStatusServicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE `solicitud` (
  `Id` int(11) NOT NULL,
  `Descripcion` varchar(250) DEFAULT NULL,
  `FechaSolicitud` datetime DEFAULT CURRENT_TIMESTAMP,
  `FechaDeseadaServicio` datetime DEFAULT NULL,
  `ComentarioAdicional` varchar(250) DEFAULT NULL,
  `IdCliente` int(11) NOT NULL,
  `IdAtendidaPor` int(11) DEFAULT NULL,
  `IdEspecialistaAsignado` int(11) DEFAULT NULL,
  `IdDireccionServicio` int(10) UNSIGNED NOT NULL,
  `IdTipoServicio` int(10) UNSIGNED NOT NULL,
  `IdSubTipoServicio` int(11) DEFAULT NULL,
  `RequiereVisita` tinyint(1) NOT NULL,
  `Sos` tinyint(1) NOT NULL,
  `StatusSolicitud` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `solicitud`
--

INSERT INTO `solicitud` (`Id`, `Descripcion`, `FechaSolicitud`, `FechaDeseadaServicio`, `ComentarioAdicional`, `IdCliente`, `IdAtendidaPor`, `IdEspecialistaAsignado`, `IdDireccionServicio`, `IdTipoServicio`, `IdSubTipoServicio`, `RequiereVisita`, `Sos`, `StatusSolicitud`) VALUES
(23, 'hacer un camino.', '2018-10-05 11:09:40', '2018-07-25 00:00:00', 'es pegar mas de 4 mst cuadrados', 1, 1, NULL, 1, 1, 1, 0, 1, 2),
(24, 'hacer un camino 2.', '2018-10-05 11:54:17', '2018-07-25 00:00:00', 'es pegar mas de 4 mst cuadrados', 1, NULL, NULL, 1, 1, 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `statusservicio`
--

CREATE TABLE `statusservicio` (
  `Id` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `statusservicio`
--

INSERT INTO `statusservicio` (`Id`, `Descripcion`) VALUES
(1, 'Cotizado'),
(2, 'En espera'),
(3, 'En camino'),
(4, 'En curso'),
(5, 'Finalizado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subtiposervicio`
--

CREATE TABLE `subtiposervicio` (
  `Id` int(11) NOT NULL,
  `Descripcion` varchar(250) NOT NULL,
  `Popularidad` int(3) NOT NULL,
  `IdTipoServicio` int(10) UNSIGNED NOT NULL,
  `VisitaObligatoria` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `subtiposervicio`
--

INSERT INTO `subtiposervicio` (`Id`, `Descripcion`, `Popularidad`, `IdTipoServicio`, `VisitaObligatoria`) VALUES
(1, 'Paredes interioerers', 0, 1, 0),
(2, 'Paredes Exteriores', 0, 1, 0),
(3, 'Puertas y/o ventanas', 0, 1, 0),
(4, 'Otro', 0, 1, 0),
(5, 'Varios', 0, 1, 0),
(6, 'Servicio de emergencia', 0, 2, 0),
(7, 'Visita y presupuesto de plomero', 0, 2, 1),
(8, 'Instalaci?n de calentador el?ctrico o estacional', 0, 2, 0),
(9, 'Cambio / instalaci?n de tarja', 0, 2, 0),
(10, 'Cambio / instalaci?n de inodoro con tanque', 0, 2, 0),
(11, 'Instalaci?n de muebles de ba?o y cocina', 0, 2, 0),
(12, 'Cambio / instalaci?n de llaves de regadera', 0, 2, 0),
(13, 'Servicio de desazolve de tuber?a simple', 0, 2, 0),
(14, 'Instalaci?n de lavadora o lavavajillas', 0, 2, 0),
(15, 'Mantenimiento de boiler', 0, 2, 0),
(16, 'Servicio de plomero por 2 horas', 0, 2, 0),
(17, 'Visita y Presupuesto de Carpintero', 0, 3, 1),
(18, 'Visita y presupuesto de fumigación', 0, 4, 1),
(19, 'Visita y Presupuesto de albañileria\r\n', 0, 5, 1),
(20, 'Visita y Cotización de Impermeabilizacion\r\n', 0, 6, 1),
(21, 'Visita y Presupuesto de Linea Blanca\r\n', 0, 7, 1),
(22, 'Visita y Presupuesto de TABLAROCA\r\n', 0, 8, 1),
(23, 'Visita y presupuesto de DUROCK', 0, 8, 1),
(24, 'Visita y presupuesto de Limpieza de mobiliario', 0, 9, 1),
(25, 'Visita y Presupuesto de Pisos\r\n', 0, 10, 1),
(26, 'Mantenimiento preventivo eléctrico (Pentafón)', 0, 11, 0),
(27, 'Visita y presupuesto de electricista', 0, 11, 1),
(28, 'Revisión de instalación eléctrica general', 0, 11, 0),
(29, 'Revisión de instalación y cambio de accesorios eléctricos', 0, 11, 0),
(30, 'Diagnóstico para servicio de Jardinería\r\n', 0, 12, 0),
(31, 'Proyecto a medida de Jardinería\r\n', 0, 12, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjeta`
--

CREATE TABLE `tarjeta` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdCliente` int(11) NOT NULL,
  `IdDireccionFacturacion` int(10) UNSIGNED NOT NULL,
  `IdClienteConekta` varchar(45) DEFAULT NULL,
  `UsarPorDefecto` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tarjeta`
--

INSERT INTO `tarjeta` (`Id`, `IdCliente`, `IdDireccionFacturacion`, `IdClienteConekta`, `UsarPorDefecto`) VALUES
(3, 1, 0, 'cus_2izWTMRvb3U8F3imY', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodireccion`
--

CREATE TABLE `tipodireccion` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Codigo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipodireccion`
--

INSERT INTO `tipodireccion` (`Id`, `Descripcion`, `Codigo`) VALUES
(1, 'casa', '0001');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipomensaje`
--

CREATE TABLE `tipomensaje` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Plantilla` varchar(45) DEFAULT NULL,
  `Vigencia` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposervicio`
--

CREATE TABLE `tiposervicio` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Popularidad` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tiposervicio`
--

INSERT INTO `tiposervicio` (`Id`, `Descripcion`, `Popularidad`) VALUES
(1, 'Pintura', 100),
(2, 'Plomeria', 100),
(3, 'Carpintero', NULL),
(4, 'Fumigacion', NULL),
(5, 'Albañil', NULL),
(6, 'Impermeabilizacion', NULL),
(7, 'Linea Blanca', NULL),
(8, 'Tabla roca y Durock', NULL),
(9, 'Limpieza de mobiliario', NULL),
(10, 'Pisos', NULL),
(11, 'Electricidad', NULL),
(12, 'Jardinero', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `token`
--

CREATE TABLE `token` (
  `Id` int(10) UNSIGNED NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  `Token` varchar(200) DEFAULT NULL,
  `Plataforma` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `token`
--

INSERT INTO `token` (`Id`, `IdUsuario`, `Token`, `Plataforma`) VALUES
(1, 1, 'fBTb2GJ_AhA:APA91bHthbFflU-R8GVBwsRCxUdaT_U5Jo1avT4bXVTPeWdg7DRQJchetO6pw37lfvKG5W2DIuAHfOlTAasEgPzHxRG7lwIok2o44XV_Jf0AuYo4nDdkClWnpoN0DhBJEVYn8l5yN0-R', 'Web'),
(2, 2, '54645645645646546546464687978', 'Web'),
(3, 8, 'e3gc_ffkje8:APA91bGwz1lr1IpbFRySaBGADwfrMDxbeWayDKyVBxbCM4r8SCwfDJYQMQvS6HFeXEy-gNt9StcYUGNX3t1RZJ_Wx9mUvzr0sVZHdV1QfG8rWbg3wHbKd_kNKv2qgb5qbz0c6Cr336aM', 'Web'),
(4, 7, 'fBDfgwfUrUY:APA91bFw06oRfJayKkbfRxReVa4yOfrwpJrQq2mC7SrKlNtVivXWZCZee9c4b6P0Zzsk_DaA5q29ry9cT9T2WZ4HgcjScdMlM2VaSemQgEha3KW9z5xFqbQRSKL47dbIFHMUiej1rw0J', 'Android');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion`
--

CREATE TABLE `transaccion` (
  `Id` int(11) NOT NULL,
  `IdTarjeta` varchar(50) NOT NULL,
  `IdServicio` int(10) UNSIGNED NOT NULL,
  `Monto` double DEFAULT NULL,
  `IdTransaccionConekta` varchar(45) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `FechaPeticion` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `transaccion`
--

INSERT INTO `transaccion` (`Id`, `IdTarjeta`, `IdServicio`, `Monto`, `IdTransaccionConekta`, `Status`, `FechaPeticion`) VALUES
(1, 'src_2izsX8WQwrp14VZoe', 1, 520.3, 'ord_2j2DwPH2Px4fAwG4e', 'paid', '2018-08-01 20:58:01'),
(2, 'src_2izsX8WQwrp14VZoe', 1, 520.3, 'ord_2j2E3Efp7NztgWYi3', 'paid', '2018-08-01 21:05:41');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `codigoEspecialista`
--
ALTER TABLE `codigoEspecialista`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `configuracionplataforma`
--
ALTER TABLE `configuracionplataforma`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `detallecotizacion`
--
ALTER TABLE `detallecotizacion`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `detalleservicio`
--
ALTER TABLE `detalleservicio`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `especialistaservicio`
--
ALTER TABLE `especialistaservicio`
  ADD PRIMARY KEY (`IdEspecialista`,`IdTipoServicio`);

--
-- Indices de la tabla `estadosservicio`
--
ALTER TABLE `estadosservicio`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `fotodetallecotizacion`
--
ALTER TABLE `fotodetallecotizacion`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `fotodetalleservicio`
--
ALTER TABLE `fotodetalleservicio`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `fotoincidencia`
--
ALTER TABLE `fotoincidencia`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`);

--
-- Indices de la tabla `fotosolicitud`
--
ALTER TABLE `fotosolicitud`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `fototiposervicio`
--
ALTER TABLE `fototiposervicio`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `historialservicio`
--
ALTER TABLE `historialservicio`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`);

--
-- Indices de la tabla `incidenciaservicio`
--
ALTER TABLE `incidenciaservicio`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`);

--
-- Indices de la tabla `statusservicio`
--
ALTER TABLE `statusservicio`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`);

--
-- Indices de la tabla `subtiposervicio`
--
ALTER TABLE `subtiposervicio`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `tipodireccion`
--
ALTER TABLE `tipodireccion`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `tipomensaje`
--
ALTER TABLE `tipomensaje`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `tiposervicio`
--
ALTER TABLE `tiposervicio`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`);

--
-- Indices de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `codigoEspecialista`
--
ALTER TABLE `codigoEspecialista`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `configuracionplataforma`
--
ALTER TABLE `configuracionplataforma`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `detallecotizacion`
--
ALTER TABLE `detallecotizacion`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalleservicio`
--
ALTER TABLE `detalleservicio`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `direccion`
--
ALTER TABLE `direccion`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `estadosservicio`
--
ALTER TABLE `estadosservicio`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `fotodetallecotizacion`
--
ALTER TABLE `fotodetallecotizacion`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `fotodetalleservicio`
--
ALTER TABLE `fotodetalleservicio`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `fotoincidencia`
--
ALTER TABLE `fotoincidencia`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `fotosolicitud`
--
ALTER TABLE `fotosolicitud`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `fototiposervicio`
--
ALTER TABLE `fototiposervicio`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `historialservicio`
--
ALTER TABLE `historialservicio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `incidenciaservicio`
--
ALTER TABLE `incidenciaservicio`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `statusservicio`
--
ALTER TABLE `statusservicio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `subtiposervicio`
--
ALTER TABLE `subtiposervicio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tipodireccion`
--
ALTER TABLE `tipodireccion`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tipomensaje`
--
ALTER TABLE `tipomensaje`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tiposervicio`
--
ALTER TABLE `tiposervicio`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `token`
--
ALTER TABLE `token`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
