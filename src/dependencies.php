<?php

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Database
$container['db'] = function($c){
    $connectionString = $c->get('settings')['connectionString'];

    $pdo = new PDO($connectionString['dns'], $connectionString['user'], $connectionString['pass']);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

    return new FluentPDO($pdo);
};

// Models
$container['model'] = function($c){
    return (object)[
        #'test'     => new App\Model\TestModel($c->db),
        'persona' => new App\Model\PersonaModel($c->db),
        'login' => new App\Model\LoginModel($c->db),
        'direccion' => new App\Model\DireccionModel($c->db),
        'especialista' => new App\Model\EspecialistaModel($c->db),
        'solicitud' => new App\Model\SolicitudModel($c->db),
        'img' => new App\Model\ImgModel($c->db),
        'tarjeta' => new App\Model\TarjetaModel($c->db),
        'cotizacion' => new App\Model\CotizacionModel($c->db),
        'detallecotizacion' => new App\Model\DetallecotizacionModel($c->db),
        'servicio' => new App\Model\ServicioModel($c->db),
        'detalleservicio' => new App\Model\DetalleservicioModel($c->db),
        'incidenciaservicio'=> new App\Model\IncidenciaservicioModel($c->db),
        'tipodireccion' => new App\Model\TipodireccionModel($c->db),
        'tiposervicio' => new App\Model\TiposervicioModel($c->db),
        'token' => new App\Model\TokenModel($c->db)
    ];
};
